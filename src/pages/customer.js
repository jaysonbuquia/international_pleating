import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

import DefaultLayout from '../layouts/default';
import CustomerForm from '../components/forms/customer';

class Customer extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DefaultLayout bodyClass="page-customer">
        <CustomerForm {...this.props}/>
      </DefaultLayout>
    )
  }
}

export default withRouter(Customer);