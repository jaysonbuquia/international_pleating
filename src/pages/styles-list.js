import React, {Component} from 'react';

import DefaultLayout from '../layouts/default';
import ContentHeading from '../components/content-heading';
import StylesGrid from '../components/styles-grid';

import SVG from '../components/svg';
import Button from '../components/button';
import styles from '../mock/styles';

class StylesList extends Component {

  constructor(props) {
    super(props)
  }

  testAction() {
    alert('test action!');
  }

  get headings() {
    return [
      {
        text: 'Styles',
        link: '/styles',
      },
      {
        text: 'Attributes',
        link: '/attributes',
      }
    ];
  }

  get buttons() {
    return [
      {
        text: 'Add new',
        active: true,
        link: '/styles/new',
      }
    ]
  }

  render() {
    return (
      <DefaultLayout bodyClass="customers-list">
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        <StylesGrid />
      </DefaultLayout>
    )
  }
}

export default StylesList;