import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

import DefaultLayout from '../layouts/default';
import StyleForm from '../components/forms/style';

class Order extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DefaultLayout bodyClass="page-style">
        <StyleForm {...this.props} />
      </DefaultLayout>
    )
  }
}

export default withRouter(Order)