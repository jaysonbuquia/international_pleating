import React, {Component, Fragment} from 'react';
import {Redirect, Link} from 'react-router-dom';

import AuthService from '../services/authService';


class Logout extends Component {

  constructor(props) {
    super(props);
  }

  render() {
  		AuthService.logout();
		return (<Redirect to='/login' />);
  }
}

export default Logout;
