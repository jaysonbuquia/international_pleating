import React, {Component} from 'react';

import CenteredLayout from '../layouts/center';
import LoginForm from '../components/forms/login';

class Login extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <CenteredLayout>
        <LoginForm />
      </CenteredLayout>
    )
  }
}

export default Login;