import React, {Component, Fragment} from 'react';
import { createPortal } from 'react-dom';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import OrderService from '../services/orderService';
import ShipmentService from '../services/shipmentService';
import DashboardService from '../services/dashboardService';
import { headerUpdateCounts } from '../services/topicsService';
import printService from '../services/printService';

import DefaultLayout from '../layouts/default';
import ContentHeading from '../components/content-heading';
import Table from '../components/table';
import SVG from '../components/svg';
import Button from '../components/button';
import OrderActions from '../components/order-actions';
import Modal from '../components/modal';
import PrintOrder from '../components/print-order';
import PrintInvoice from '../components/print-invoice';


const body = document.querySelector('body');


class OrdersList extends Component {

  constructor(props) {
    super(props);

    this.state = {

      // Pre-filtered items to show in the list
      items: [],

      // UI state, while fetching for items
      fetching: true,

      // Current item to use by other components, such as modal
      selectedItem: null,

      // Search query
      search: null,

    }
  }

  showSearch() {
    this.searchModal.show();
  }

  clearSearch() {
    this.setState({ search: null });
  }

  get headings() {
    if ( !(/^\/(orders|history)$/).test(this.props.location.pathname) ) {
      return [];
    } else {
      return [
        {
          text: 'Orders',
          link: '/orders',
        },
        {
          text: 'History',
          link: '/history',
        }
      ];
    }
  }

  get buttons() {

    const buttons = [];

    if ( this.state.search ) {
      buttons.push({
        text: 'Clear Search',
        action: this.clearSearch.bind(this),
      });
    }

    return buttons.concat([
      {
        text: 'Search',
        action: this.showSearch.bind(this),
      },
      {
        text: 'Add new',
        active: true,
        link: '/orders/new',
      }
    ]);
  }

  get tableHeadings() {
    return [
      'Order #',
      'Order Date',
      'Complete Date',
      'Color',
      'QTY',
      'Price',
      'Company',
      'Status',
      null, // Empty heading
    ];
  }


  get tableRows() {
    let items = [...this.state.items];

    // Filter items if there is search query
    if ( this.state.search ) {
      const searchRegex = new RegExp(this.state.search,'ig');
      items = items.filter( ({order_number, customer, ...item}) => {
        return (searchRegex.test(customer.name) || searchRegex.test(order_number)) ? true : false;
      });
    }

    return items.map( item => {
      let {id, order_number, order_date, completion_date = null, style, color, customer, quantity, status } = item;
      order_date = new Date(order_date);
      completion_date = completion_date ? new Date(completion_date) : null;
      return [
        order_number,
        this.formatDate(order_date),
        this.formatDate(completion_date),
        ( color ? <div className="orders-table__color"><i style={{backgroundColor: `${color.color_value}`}}></i>{color.color_name}</div>: null),
        this.getQuantity(item),
        `$ ${style.price}`,
        (customer ? customer.name : null ),
        (<div className="orders-table__status" data-status={status}>{this.getStatusText(status)}</div>),
        this.isHistoryPage ? null : this.renderActionButtons( item ),
      ];
    });
  }


  get content() {
    if (this.state.fetching) {
      return (<div className="orders-list__loading"><SVG name='loading' /> Fetching Orders...</div>)
    }
    else if ( !this.state.items.length ) {
      return (<div className="orders-list__empty">No data available.</div>)
    } else {
      return (<Table className='orders-table' headings={this.tableHeadings} rows={this.tableRows} />)
    }
  }


  printOrderOrInvoice(item) {

    // For orders pages (orders, productions), print an order page
    if ( !this.isShipmentsPage ) {
      return printService.printOrder(item);
    }

    // For shipments page, print an invoice
    else {
      let order = item.originalOrder;
      let customer = order.customer;
      let shipments = this.state.items.filter( shipment => shipment.customer.id === customer.id );
      console.log('shipments',shipments);
      // Group by order id
      let order_ids = [];
      let orders = [];
      for ( let shipment of shipments ) {
        if ( !(order_ids.includes(shipment.originalOrder.id)) ) {
          order_ids.push(shipment.originalOrder.id);
          orders.push(shipment.originalOrder);
        }
      }
      return printService.printInvoice(orders);
    }

  }

  renderActionButtons( item ) {
    return (<Fragment>
      <OrderActions item={item} parentOrdersList={this}/>
      <button className="orders-table__print" onClick={ e => this.printOrderOrInvoice(item) }></button>
    </Fragment>)
  }

  getQuantity( order ) {
    switch ( this.props.location.pathname ) {
      case '/productions':
        return order.quantity_production;
      default:
        return order.quantity;
    }
  }

  getStatusText( status ) {
    switch (status) {
      case 'new':
        return 'New Order';
      case 'production':
        return 'In Production';
      case 'hold':
        return 'On Hold';
      case 'shipment':
        return 'Ready'
      case 'done':
        return 'Done';
      case 'cancelled':
        return 'Cancelled';
      default:
        return '';
    }
  }

  async proceedSearch(e) {
    e.preventDefault();

    let search = this.searchInput.value;

    this.searchModal.hide();
    this.setState({ search });
  }

  // Formats a date into MM/DD/YY (non-zero-prefixed)
  formatDate(date) {
    if ( !date ) {
      return '';
    }

    return [
      date.getMonth(),
      date.getDate(),
      date.getFullYear().toString().slice(-2),
    ].join('/');
  }

  async fetchListData(search) {
    let items = [];

    // Update header counts
    DashboardService.updateHeaderCounts();

    // Fetch items
    await this.setState({ fetching: true });

    // Fetch shipments on '/shipments' page
    if ( this.isShipmentsPage ) {
      items = await ShipmentService.all(search);
      // Ensure to use the order reference and not the shipment
      items = items.map( ({order, quantity}) => { return {...order, quantity, originalOrder: order} } );
    }
    // Fetch orders on all other orders list pages
    else {
      let status = this.orderStatus;
      items = await OrderService.withStatus({status, search});
    }

    printService.use(items[0]);
    await this.setStateAsync({ items, fetching: false });
  }

  get orderStatus() {
    switch (this.props.location.pathname) {
      case '/productions':
        return 'production,hold';
      case '/history':
        return 'done,cancelled';
      default:
        return 'new';
    }
  }

  get printComponent() {
    return this.isShipmentsPage ? <PrintInvoice /> : <PrintOrder />;
  }

  get isHistoryPage() {
    return /^\/history$/.test(this.props.location.pathname);
  }

  get isShipmentsPage() {
    return /\/shipments/i.test(this.props.location.pathname);
  }

  async componentWillMount() {
    this.mounted = true;
    this.fetchListData();
    this.unlisten = this.props.history.listen(async (location, action) => {
      this.fetchListData();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
    this.unlisten();
  }

  render() {
    return (
      <DefaultLayout bodyClass="orders-list">
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        {this.content}

        <Modal ref={ ref => this.searchModal = ref }>
          <div className="modal__heading">Search for order:</div>
          <div className="modal__content">
              <div className="modal__form">
                <input type="text" name="search" className="modal__input" ref={ref => this.searchInput = ref} />
                <div className="modal__actions">
                  <Button text="Search" className="btn modal__proceed" onClick={this.proceedSearch.bind(this)} />
                </div>
              </div>
          </div>
        </Modal>

        {createPortal(this.printComponent, body)}

      </DefaultLayout>
    )
  }
}

export default withRouter(OrdersList);