import React, {Component} from 'react';

import CenteredLayout from '../layouts/center';

class Login extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="page-loading">
        {/* Inline SVG allows it to show up immediately without waiting for the sprite asset to be downloaded */}
        <svg className="page-loading__icon" xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26.3 26.4"><circle cx="13.8" cy="3.1" r="3.1"/><circle cx="13.8" cy="24.5" r="1.8"/><circle cx="6.2" cy="6.2" r="2.8"/><circle cx="21.4" cy="21.4" r="1.5"/><circle cx="3.1" cy="13.8" r="2.5"/><circle cx="24.5" cy="13.8" r="1.2"/><path d="M4.7 19.8c-0.8 0.8-0.8 2.2 0 3.1 0.8 0.8 2.2 0.8 3.1 0 0.8-0.8 0.8-2.2 0-3C6.9 19 5.5 19 4.7 19.8z"/><circle cx="21.4" cy="6.2" r="0.9"/></svg>
      </div>
    )
  }
}

export default Login;