import React, {Component, Fragment} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import attributeService from '../services/attributeService';
import { headerUpdateCounts } from '../services/topicsService';

import DefaultLayout from '../layouts/default';
import ContentHeading from '../components/content-heading';
import Table from '../components/table';
import SVG from '../components/svg';
import Button from '../components/button';
import AttributeActions from '../components/attribute-actions';
import Modal from '../components/modal';

class AttributesList extends Component {

  constructor(props) {
    super(props)

    this.state = {

      // Pre-filtered items to show in the list
      items: [],

      // UI state, while fetching for items
      fetching: true,

      // Will contain form errors
      formErrors: [],

    }
  }

  get headings() {
    return [
      {
        text: 'Styles',
        link: '/styles',
      },
      {
        text: 'Attributes',
        link: '/attributes',
      }
    ];
  }

  get buttons() {
    return [
      {
        text: 'Add new',
        active: true,
        action:  (e) => this.showAddModal(e)
      }
    ]
  }

  get tableHeadings() {
    return [
      'Attribute Name',
      null, // Empty heading
    ];
  }

  get tableRows() {
    return this.state.items.map( item => {

      return [
        item.name,
        this.renderActionButtons( item ),
      ];
    });
  }

  showAddModal(e) {
    e.preventDefault();
    this.addModal.show();
  }

  hideAddModal() {
    this.addModal.hide();
  }

  async proceedSave() {
    const name = this.nameInput.value;

    attributeService.use({ name });
    this.savingModal.show();

    try {

      const response = await attributeService.save();

      let savedAttribute = response.data;
      this.savingModal.hide();
      this.hideAddModal();
      this.savedModal.show();

      setTimeout(() => {
        this.savedModal.hide();
      }, 2300);

      if ( savedAttribute ) {
        setTimeout(() => {
          this.fetchListData();
        }, 2600);
      }

    } catch (err) {
      console.warn(err);
      this.setState({ formErrors: err.errors });
      this.savingModal.hide();
      this.formErrorModal.show();
      return;
    }
  }

  renderActionButtons( item ) {
    return (
      <Fragment>
        {<AttributeActions item={item} parentAttributesList={this} />}
      </Fragment>
    )
  }

  async fetchListData() {
    let items = [];

    // Update header counts
    publish(headerUpdateCounts);
    await this.setState({ fetching: true });

    // Fetch items
    items = await attributeService.all();

    this.setState({ items, fetching: false });
  }

  async componentWillMount() {
    this.fetchListData();
  }

  get content() {
    if (this.state.fetching) {
      return (<div className="attributes-list__loading"><SVG name='loading' /> Fetching Attributes...</div>)
    }
    else if ( !this.state.items.length ) {
      return (<div className="attributes-list__empty">No data available.</div>)
    } else {
      return (<Table className='attributes-table' headings={this.tableHeadings} rows={this.tableRows} />)
    }
  }

  render() {
    return (
      <DefaultLayout bodyClass="attributes-list">
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        {this.content}

        {/* ATTRIBUTE FORM */}
        <Modal ref={ ref => this.addModal = ref } className="attribute-form-modal">
          <div className="modal__heading">New attribute</div>
          <div className="modal__content">
              <div className="modal__form">
                <input type="text" name="name" className="modal__input" ref={ref => this.nameInput = ref} />
                <div className="modal__actions">
                  <Button text="Cancel" className="btn modal__dismiss" onClick={this.hideAddModal.bind(this)} />
                  <Button text="Save" className="btn modal__proceed" onClick={this.proceedSave.bind(this)} />
                </div>
              </div>
          </div>
        </Modal>

        {/* FORM ERROR MODAL */}
        <Modal ref={ ref => this.formErrorModal = ref } >
          <div className="modal__heading">Please fix the following error(s):</div>
          <div className="modal__text">
              {Object.keys(this.state.formErrors).map( (error, key) => {
                return (
                  <div key={key} >{this.state.formErrors[error][0]}</div>
                )
              })}
          </div>
        </Modal>

        {/* SAVING MODAL */}
        <Modal ref={ ref => this.savingModal = ref } persistent={true} >
          <div className="modal__heading">Saving Attribute...</div>
        </Modal>

        {/* SAVED MODAL */}
        <Modal ref={ ref => this.savedModal = ref } persistent={true} >
          <div className="modal__heading">Saved!</div>
        </Modal>
      </DefaultLayout>
    )
  }
}

export default withRouter(AttributesList);