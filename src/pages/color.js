import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';

import DefaultLayout from '../layouts/default';
import ColorForm from '../components/forms/color';

class Color extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DefaultLayout bodyClass="new-color">
        <ColorForm {...this.props}/>
      </DefaultLayout>
    )
  }
}

export default withRouter(Color)