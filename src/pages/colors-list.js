import React, {Component, Fragment} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import ColorService from '../services/colorService';
import { headerUpdateCounts } from '../services/topicsService';

import DefaultLayout from '../layouts/default';
import ContentHeading from '../components/content-heading';
import Table from '../components/table';
import SVG from '../components/svg';
import Button from '../components/button';
import ColorActions from '../components/color-actions';

class ColorsList extends Component {

  constructor(props) {
    super(props)

    this.state = {

      // Pre-filtered items to show in the list
      items: [],

      // UI state, while fetching for items
      fetching: true,

    }
  }

  get headings() {
    return [
      {
        text: 'Colors',
        link: '/colors',
      }
    ];
  }

  get buttons() {
    return [
      {
        text: 'Add new',
        active: true,
        link: '/colors/new',
      }
    ]
  }

  get tableHeadings() {
    return [
      'Color Number',
      'Color Name',
      'Color Value',
      null, // Empty heading
    ];
  }

  get tableRows() {

    console.log('this.state.items',this.state.items);

    // During integration, this should process raw order data into presentation elements
    return this.state.items.map( item => {

      const { color_number, color_value, color_name } = item;

      return [
        color_number,
        <span>
          <div className="color_preview" style={{backgroundColor: color_value}}></div>
          {color_name}
        </span>,
        color_value,
        this.renderActionButtons( item ),
      ];
    });
  }

  renderActionButtons( item ) {
    return (
      <Fragment>
        <ColorActions item={item} parentColorsList={this} />
      </Fragment>
    )
  }

  // Formats a date into MM/DD/YY (non-zero-prefixed)
  formatDate(date) {
    return [
      date.getMonth(),
      date.getDate(),
      date.getFullYear().toString().slice(-2),
    ].join('/');
  }

  async fetchListData() {
    let items = [];

    // Update header counts
    publish(headerUpdateCounts);
    await this.setState({ fetching: true });

    // Fetch items
    items = await ColorService.all();

    this.setState({ items, fetching: false });
  }

  async componentWillMount() {
    this.mounted = true;
    this.fetchListData();
    this.unlisten = this.props.history.listen(async (location, action) => {
      this.fetchListData();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
    this.unlisten();
  }

  get content() {
    if (this.state.fetching) {
      return (<div className="colors-list__loading"><SVG name='loading' /> Fetching Colors...</div>)
    }
    else if ( !this.state.items.length ) {
      return (<div className="colors-list__empty">No data available.</div>)
    } else {
      return (<Table className='colors-table' headings={this.tableHeadings} rows={this.tableRows} />)
    }
  }

  render() {
    return (
      <DefaultLayout bodyClass="colors-list">
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        {this.content}
      </DefaultLayout>
    )
  }
}

export default withRouter(ColorsList);