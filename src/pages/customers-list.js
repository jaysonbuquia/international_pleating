import React, {Component, Fragment} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import CustomerService from '../services/customerService';
import { headerUpdateCounts } from '../services/topicsService';

import DefaultLayout from '../layouts/default';
import ContentHeading from '../components/content-heading';
import Table from '../components/table';
import SVG from '../components/svg';
import Button from '../components/button';
import customers from '../mock/customers';
import CustomerActions from '../components/customer-actions';
import Modal from '../components/modal';

class CustomersList extends Component {

  constructor(props) {
    super(props)

    this.state = {

      // Pre-filtered items to show in the list
      items: [],

      // UI state, while fetching for items
      fetching: true,

      // Will hold search query
      search: null,

    }
  }

  showSearch() {
    this.searchModal.show();
  }

  clearSearch() {
    this.setState({ search: null });
  }

  async proceedSearch(e) {
    e.preventDefault();

    let search = this.searchInput.value;

    this.searchModal.hide();
    this.setState({ search });
  }

  get headings() {
    return [
      {
        text: 'Customers',
        link: '/customers',
      }
    ];
  }

  get buttons() {
    const buttons = [];

    if ( this.state.search ) {
      buttons.push({
        text: 'Clear Search',
        action: this.clearSearch.bind(this),
      });
    }

    return buttons.concat([
        {
          text: 'Search',
          action: this.showSearch.bind(this),
        },
        {
          text: 'Add new',
          active: true,
          link: '/customers/new',
        }
      ]);
  }

  get tableHeadings() {
    return [
      'Acc #',
      'Company Name',
      'Contact Name',
      'State',
      'Telephone',
      'Email',
      'Notes',
      null, // Empty heading
    ];
  }

  get tableRows() {
    let items = [...this.state.items];

    // Filter items if there is search query
    if ( this.state.search ) {
      const searchRegex = new RegExp(this.state.search,'ig');
      items = items.filter( item => searchRegex.test(item.name));
    }

    return items.map( item => {

      const { account_number, name, contact, state, phone, email, notes } = item;

      return [
        account_number,
        name,
        contact,
        state,
        phone ? (<a href={`tel:${phone}`}>{phone}</a>) : '',
        email ? (<a href={`mailto:${email}`}>{email}</a>) : '',,
        notes ? (<span className="customers-list__notes"><SVG name='flag'/>{notes}</span>) : '',
        this.renderActionButtons( item ),
      ];
    });
  }

  get content() {
    if (this.state.fetching) {
      return (<div className="orders-list__loading"><SVG name='loading' /> Fetching Customers...</div>)
    }
    else if ( !this.state.items.length ) {
      return (<div className="orders-list__empty">No data available.</div>)
    } else {
      return (<Table className='customers-table' headings={this.tableHeadings} rows={this.tableRows} />)
    }
  }

  renderActionButtons( item ) {
    return (<Fragment>
      <CustomerActions item={item} parentOrdersList={this}/>
      <button className="orders-table__print" data-order-id={item.id}></button>
    </Fragment>)
  }

  getStatusText( status ) {
    switch (status) {
      case 'new':
        return 'New Order';
      case 'printing':
        return 'In Production';
      case 'shipped':
        return 'Complete';
      default:
        return '';
    }
  }

  // Formats a date into MM/DD/YY (non-zero-prefixed)
  formatDate(date) {
    return [
      date.getMonth(),
      date.getDate(),
      date.getFullYear().toString().slice(-2),
    ].join('/');
  }

  async fetchListData() {
    let items = [];

    // Update header counts
    publish(headerUpdateCounts);
    await this.setState({ fetching: true });

    // Fetch items
    items = await CustomerService.all();

    this.setState({ items, fetching: false });
  }

  async componentWillMount() {
    this.mounted = true;
    this.fetchListData();
    this.unlisten = this.props.history.listen(async (location, action) => {
      this.fetchListData();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
    this.unlisten();
  }

  render() {
    return (
      <DefaultLayout bodyClass="customers-list">
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        {this.content}

        <Modal ref={ ref => this.searchModal = ref }>
          <div className="modal__heading">Search for customer:</div>
          <div className="modal__content">
              <div className="modal__form">
                <input type="text" name="search" className="modal__input" ref={ref => this.searchInput = ref} />
                <div className="modal__actions">
                  <Button text="Search" className="btn modal__proceed" onClick={this.proceedSearch.bind(this)} />
                </div>
              </div>
          </div>
        </Modal>
      </DefaultLayout>
    )
  }
}

export default withRouter(CustomersList);