import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

import DefaultLayout from '../layouts/default';
import OrderForm from '../components/forms/order';

class Order extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DefaultLayout bodyClass="page-order">
        <OrderForm {...this.props}/>
      </DefaultLayout>
    )
  }
}

export default withRouter(Order);