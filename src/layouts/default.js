import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import { subscribe } from 'pubsub-js';

import userDataResponse from '../services/topicsService';
import AuthService from '../services/authService';

import Wrapper from './wrapper';
import Header from '../components/header';
import PageLoading from '../pages/loading';

class DefaultLayout extends Component {

  constructor(props) {
    super(props);

    this.state = {

      userData: null,
    }

    subscribe(userDataResponse, this.onUserDataResponse.bind(this));
  }

  async getUserData() {
    try {
      const userData = await AuthService.getUserData();

      if ( userData ) {
        this.setState({userData});
      }
    } catch (err) {
      console.error(err);
      this.props.history.push('/login');
    }
  }

  onUserDataResponse(action, userData) {
    this.setState({ userData });
  }

  render() {
    let { children, bodyClass, ...otherProps } = this.props;

    if ( !this.state.userData ) {
      this.getUserData();
      return <PageLoading />
    }

    return (
      <Wrapper className={`layout-default ${bodyClass}`}>
        <Header />
        <div className="content">
          {children}
        </div>
      </Wrapper>
    )
  }

}

export default withRouter(DefaultLayout);