import React, {Component} from 'react';

import Wrapper from './wrapper';

class CenteredLayout extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let { children, ...otherProps } = this.props;

    return (
      <Wrapper className="layout-centered">
        {children}
      </Wrapper>
    )
  }

}

export default CenteredLayout;