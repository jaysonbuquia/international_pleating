import React, { Component, Fragment } from 'react';

/**
 * This component only adds to the HTML Body,
 * it does not replace the body itself
 */

class Wrapper extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let { children, className } = this.props;

    return (
      <div className={`wrapper ${className ? className : ''}`}>
        {children}
      </div>
    )
  }

}

export default Wrapper;