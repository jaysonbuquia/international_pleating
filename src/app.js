import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import hashHistory from 'history/createBrowserHistory';
import './tools/decorateReactComponent';

// Import fetch polyfill
import 'whatwg-fetch';

import AuthService from './services/authService';

import Login from './pages/login';
import Logout from './pages/logout';
import Home from './pages/home';
import OrdersList from './pages/orders-list';
import CustomersList from './pages/customers-list';
import StylesList from './pages/styles-list';
import AttributesList from './pages/attributes-list';
import ColorsList from './pages/colors-list';
import Color from './pages/color';
import Customer from './pages/customer';
import Order from './pages/order';
import Style from './pages/style';
import PageLoading from './pages/loading';
import SVG from './components/svg';

/**
 * ProtectedRoute component will require authentication in order to access
 * otherwise, user will be redirectred to the login page
 */
const ProtectedRoute = ({ component: Component, ...props }) => {

  const _component = <Component {...props} />;
  const _redirect = <Redirect to="/login" />;

  if ( AuthService.isLoggedIn ) {
    AuthService.getUserData();
    return _component;
  } else {
    return _redirect;
  }
}

export default class App extends Component {
  constructor(props) {
    super(props);
  }
  async componentDidUpdate() {
    // AuthService.updateUserData();
  }
  render() {
    return (
      <Router history={hashHistory()}>
        <Switch>
          <Route path="/" exact render={() => <Redirect to='/orders' />} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <ProtectedRoute path="/orders/new" exact component={Order} />
          <ProtectedRoute path="/orders/:order_id" component={Order} />
          <ProtectedRoute path="/orders" exact component={OrdersList} />
          <ProtectedRoute path="/history" component={OrdersList} />
          <ProtectedRoute path="/productions" component={OrdersList} />
          <ProtectedRoute path="/shipments" component={OrdersList} />
          <ProtectedRoute path="/customers/new" exact component={Customer} />
          <ProtectedRoute path="/customers/:customer_id" component={Customer} />
          <ProtectedRoute path="/customers" component={CustomersList} />
          <ProtectedRoute path="/styles/new" exact component={Style} />
          <ProtectedRoute path="/styles/:style_id" component={Style} />
          <ProtectedRoute path="/styles" component={StylesList} />
          <ProtectedRoute path="/attributes" exact component={AttributesList} />
          <ProtectedRoute path="/colors/new" exact component={Color} />
          <ProtectedRoute path="/colors/:color_id" component={Color} />
          <ProtectedRoute path="/colors" exact component={ColorsList} />
        </Switch>
      </Router>
    )
  }
}

render(<App />, document.getElementById('root'));