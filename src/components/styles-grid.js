import React, {Component, Fragment} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';
import Flickity from 'flickity';

import StyleService from '../services/styleService';
import { headerUpdateCounts } from '../services/topicsService';

import StyleCardActions from '../components/style-actions';
import SVG from '../components/svg';

class StylesGrid extends Component {

  constructor(props) {
    super(props);

    this.state = {

      // Pre-filtered items to show in the list
      items: [],

      // UI state, while fetching for items
      fetching: true,

    }

    this.imageCarousels = [];
  }

  async componentWillMount() {
    this.mounted = true;
    this.fetchListData();
    this.unlisten = this.props.history.listen(async (location, action) => {
      this.fetchListData();
    });
  }

  initializeCaorousels() {
    this.imageCarousels.forEach( carousel => {
      new Flickity(carousel, {
        prevNextButtons: false,
      });
    });
  }

  async fetchListData() {
    let items = [];

    // Update header counts
    publish(headerUpdateCounts);
    await this.setState({ fetching: true });

    // Fetch items
    items = await StyleService.all();

    await this.setStateAsync({ items, fetching: false });

    this.initializeCaorousels();
  }

  renderStyleImages (imagesArray) {
    return imagesArray.map( (image, key) => (
      <div className="style-card__image" key={key}>
        <img src={image.src} alt=""/>
      </div>
    ));
  }

  componentWillUnmount() {
    this.mounted = false;
    this.unlisten();
  }

  get styleCards() {

    if (this.state.fetching) {
      return (<div className="styles-grid__loading"><SVG name='loading' /> Fetching Styles...</div>)
    }
    else if ( !this.state.items.length ) {
      return (<div className="styles-grid__empty">No data available.</div>)
    } else {
      return this.state.items.map( (style, key) => (
        <div className="style-card" key={key} ref=''>
          <StyleCardActions item={style} parentStylesList={this} />
          <div className="style-card__images-container">
            <div className="style-card__images" ref={ el => this.imageCarousels.push(el) }>
              {this.renderStyleImages(style.images)}
            </div>
          </div>
          <div className="style-card__info">
            <Link to={`/styles/${style.id}`}>
              <div className="style-card__number">#{style.number}</div>
              <div className="style-card__name">{style.name}</div>
              <div className="style-card__price">${style.price}</div>
            </Link>
          </div>
        </div>
      ));
    }
  }

  render() {
    return (
      <div className="styles-grid">
        {this.styleCards}
      </div>
    )
  }
}

export default withRouter(StylesGrid)