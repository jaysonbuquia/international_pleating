import React, {Component} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import attributeService from '../services/attributeService';

import Modal from '../components/modal';
import SVG from '../components/svg';
import Button from '../components/button';

class AttributeActions extends Component {

  constructor(props) {
    super(props);
    this.state = {

      // Flag if this component should show up
      isOpen: false,

      // Will contain form errors
      formErrors: [],

    };

    document.querySelector('body').addEventListener('click', e => {
      if (this.mounted) {
        this.setState({isOpen: false});
      }
    });
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  onClick(e) {
    e.stopPropagation();
    this.setState({isOpen: !this.state.isOpen});
  }

  confirmDelete(e) {
    e.preventDefault();
    this.confirmDeleteModal.show();
  }

  async proceedDelete() {
    this.confirmDeleteModal.hide();
    this.deletingModal.show();

    let response = await attributeService.delete(this.props.item.id);

    this.deletingModal.hide();
    this.showDeletedModal();
  }

  showDeletedModal() {
    this.deletedModal.show();
    setTimeout(() => {
      this.deletedModal.hide();
    }, 2000);
    setTimeout(() => {
      this.props.parentAttributesList.fetchListData();
    }, 2300);
  }

  showEditModal(e) {
    e.preventDefault();
    this.editModal.show();
  }

  hideEditModal() {
    this.editModal.hide();
  }

  async proceedSave() {
    const id = this.props.item.id;
    const name = this.nameInput.value;

    attributeService.use({ name, id });
    this.savingModal.show();

    try {

      const response = await attributeService.update();

      let savedAttribute = response.data;
      this.savingModal.hide();
      this.hideEditModal();
      this.savedModal.show();

      setTimeout(() => {
        this.savedModal.hide();
      }, 2300);

      if ( savedAttribute ) {
        setTimeout(() => {
          this.props.parentAttributesList.fetchListData();
        }, 2600);
      }

    } catch (err) {
      console.warn(err);
      this.setState({ formErrors: err.errors });
      this.savingModal.hide();
      this.formErrorModal.show();
      return;
    }
  }

  get actions() {
    const { item } = this.props;
    const { id, name } = item;

    return [
      <a href="#" onClick={this.showEditModal.bind(this)} className="attributes-table__action" key="edit"><span>Edit attribute</span></a>,
      <a href="#" onClick={this.confirmDelete.bind(this)} className="attributes-table__action" key="delete"><span>Delete this attribute</span></a>
    ]
  }

  render() {
    const { item } = this.props;
    const { id, name } = item;

    return (
      <button className='attributes-table__actions' onClick={this.onClick.bind(this)}>
        <div className={`attributes-table__actions__popup ${ this.state.isOpen ? 'visible' : '' }`}>
          {this.actions}
        </div>

        {/* EDIT FORM */}
        <Modal ref={ ref => this.editModal = ref } className="attribute-form-modal">
          <div className="modal__heading">Edit attribute name:</div>
          <div className="modal__content">
              <div className="modal__form">
                <input type="text" name="name" defaultValue={name} className="modal__input" ref={ref => this.nameInput = ref} />
                <div className="modal__actions">
                  <Button text="Cancel" className="btn modal__dismiss" onClick={this.hideEditModal.bind(this)} />
                  <Button text="Save" className="btn modal__proceed" onClick={this.proceedSave.bind(this)} />
                </div>
              </div>
          </div>
        </Modal>

        {/* FORM ERROR MODAL */}
        <Modal ref={ ref => this.formErrorModal = ref } >
          <div className="modal__heading">Please fix the following error(s):</div>
          <div className="modal__text">
              {Object.keys(this.state.formErrors).map( (error, key) => {
                return (
                  <div key={key} >{this.state.formErrors[error][0]}</div>
                )
              })}
          </div>
        </Modal>

        {/* SAVING MODAL */}
        <Modal ref={ ref => this.savingModal = ref } persistent={true} >
          <div className="modal__heading">Saving Attribute...</div>
        </Modal>

        {/* SAVED MODAL */}
        <Modal ref={ ref => this.savedModal = ref } persistent={true} >
          <div className="modal__heading">Saved!</div>
        </Modal>

        {/* CONFIRM DELETE MODAL */}
        <Modal ref={ ref => this.confirmDeleteModal = ref } >
          <h3 className="modal__heading">{name}</h3>
          <div className="modal__text">Are you sure you want to delete this attribute?</div>
          <div className="modal__actions">
            <Button className="btn modal__dismiss" text="No, don’t delete" onClick={ e => this.confirmDeleteModal.hide() }></Button>
            <Button className="btn modal__proceed" text="Yes, delete" onClick={this.proceedDelete.bind(this)}></Button>
          </div>
        </Modal>

        {/* DELETING STATE MODAL */}
        <Modal ref={ ref => this.deletingModal = ref } persistent={true}>
          <h3 className="modal__heading">Deleting Attribute {name}</h3>
          <div className="modal__text">Please wait...</div>
        </Modal>

        {/* DELETED STATE MODAL */}
        <Modal ref={ ref => this.deletedModal = ref } persistent={true}>
          <h3 className="modal__heading">{name}</h3>
          <div className="modal__text">Has been deleted.</div>
          <img src="/images/trash-can.png" alt="" className="modal__graphic"/>
        </Modal>

      </button>
    )
  }
}

export default withRouter(AttributeActions)
