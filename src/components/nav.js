import React, {Component} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { subscribe } from 'pubsub-js';

import { headerUpdateCounts } from '../services/topicsService';
import apiService from '../services/apiService';
import DashboardService from '../services/dashboardService';

import SVG from "./svg";

class Nav extends Component {

  constructor(props) {
    super(props);

    this.state = {

      // Navigation items
      navData: {
        orders: {
          text: 'Orders',
          path: '/orders',
          icon: 'dollar',
          count: 0,
        },
        productions: {
          text: 'Productions',
          path: '/productions',
          icon: 'check',
          count: 0,
        },
        shipments: {
          text: 'Shipments',
          path: '/shipments',
          icon: 'box',
          count: 0,
        },
        customers: {
          text: 'Customers',
          path: '/customers',
          icon: 'user-solid',
        },
        styles: {
          text: 'Styles',
          path: '/styles',
          icon: 'star-solid',
        },
        colors: {
          text: 'Colors',
          path: '/colors',
          icon: 'shield',
        }
      }
    }
  }

  componentDidMount() {
    subscribe(headerUpdateCounts, this.updateCounts.bind(this) );
    DashboardService.updateHeaderCounts();
  }

  updateCounts( msg, counts ) {
    let navData = {...this.state.navData};

    if ( counts ) {
      // Update counts
      navData.orders.count = counts.new_orders;
      navData.productions.count = counts.productions;
      navData.shipments.count = counts.shipments;

      this.setState({ navData });
    }
  }

  // Generates a list of navigation elements
  get navItems() {
     return Object.keys(this.state.navData).map(( navKey, key ) => {

      const { text, path, icon = false, count = false } = this.state.navData[navKey];

      let iconElement = !icon ? null : (
        <div className="nav__graphic__icon">
          <SVG name={icon} />
        </div>
      )

      let countElement = !count ? null : (
        <span className="nav__badge">{count}</span>
      )

      return (
        <li className="nav__item" key={key}>
          <Link to={path} className="nav__link">
            <div className="nav__graphic">{iconElement} {countElement}</div>
            <span className="nav__text">{text}</span>
          </Link>
        </li>
      )
    });
  }

  render() {
    return (
      <nav className="nav">
        <ul className="nav__content">
          {this.navItems}
        </ul>
      </nav>
    )
  }
}

export default withRouter(Nav);