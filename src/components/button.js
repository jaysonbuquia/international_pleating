import React, {Component, Fragment} from 'react';

import SVG from './svg';

class Button extends Component {

  get leftIcon() {
    let { icon, iconLocation = 'left' } = this.props;

    if ( !icon || iconLocation !== 'left' ) {
      return null;
    } else {
      return <SVG name={icon} className="btn__icon btn__icon--left" />
    }
  }

  get rightIcon() {
    let { type = 'button', icon, iconLocation = 'left' } = this.props;

    if ( type ==='submit' ) {
      return (
        <Fragment>
          <SVG name="check" className="validation-passed" />
          <SVG name="times" className="validation-failed" />
        </Fragment>
      )
    }

    if ( !icon || iconLocation !== 'right' ) {
      return null;
    } else {
      return <SVG name={icon} className="btn__icon btn__icon--right" />
    }
  }

  render() {

    let { icon, iconLocation = 'left', text, type = 'button', className, ...rest } = this.props;

    return (
      <button className={className} type={type} {...rest}>
        {this.leftIcon}
        <span>{text}</span>
        {this.rightIcon}
      </button>
    )
  }

}

export default Button;