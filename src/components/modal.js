import React, {Component} from 'react';
import { createPortal } from 'react-dom';

import SVG from './svg';

const BODY = document.querySelector('body');

class Modal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      opaque: false,
    };
  }

  onModalCloseClick(e) {
    e.preventDefault();
    this.hide();
  }

  async show() {
    await this.setStateAsync({ visible: true });
    setTimeout(() => this.setState({ opaque: true }), 1);
  }

  async hide() {
    await this.setStateAsync({ opaque: false });
    setTimeout(() => this.setState({ visible: false }), 300);
  }

  render() {
    return createPortal(
       <dialog className={`modal ${this.props.className} ${this.state.opaque ? 'visible' : ''}`} ref={ el => this.element = el } open={this.state.visible}>
        <div className="modal__underlay"></div>
        <div className="modal__box">
          {
            !this.props.persistent
            ? <button className="modal__close" onClick={this.onModalCloseClick.bind(this)}><SVG name="times" /></button>
            : null
          }
          <div className="modal__content">
            {this.props.children}
          </div>
        </div>
      </dialog>
    , BODY)
  }
}

export default Modal