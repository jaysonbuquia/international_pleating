import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { subscribe, publish } from 'pubsub-js';

import userDataResponse from '../services/topicsService';
import AuthService from '../services/authService';

import UserBlock from './user-block';
import Nav from './nav';

class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {

      // Will contain user data
      userData: null,
    }

  }

  async componentWillMount() {
    try {
      const userData = await AuthService.getUserData();

      if ( !userData.photo || !userData.photo.src ) {
        userData.photo = {
          src: '/images/user.png' // Fallback image
        };
      }

      this.setState({ userData });

    // Auth failed, redirect to login
    } catch (err) {
      console.warn(err.message);
      AuthService.removeToken();
      this.props.history.replace('/login');
    }

  }

  render() {
    let { children, ...otherProps } = this.props;

    return (
      <header className="main-header">
        <div className="main-header__top">
          <a href="/" className="brand">
            <span className="brand__title">INTERNATIONAL PLEATING</span>
            <small className="brand__version">v1.0</small>
          </a>
          { this.state.userData ? <UserBlock user={this.state.userData} /> : null }
        </div>
        <div className="main-header__bottom">
          <Nav />
        </div>
      </header>
    )
  }

}

export default withRouter(Header);