import React, {Component} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import orderService from '../services/orderService';
import shipmentService from '../services/shipmentService';

import Modal from '../components/modal';
import SVG from '../components/svg';
import Button from '../components/button';

// Determine for each status which actions should be available
const STATUS_ACTIONS = {
  new: [ 'toProductions', 'edit','delete' ],
  production: [ 'done', 'toShipments', 'hold', 'edit', 'delete'],
  hold: [ 'done', 'toProductions', 'edit', 'delete'],
  shipment: [ 'done', 'edit', 'delete' ],
  done: [],
  cancelled: [],
};

class OrderActions extends Component {

  constructor(props) {
    super(props);
    this.state = {

      // Flag if this component should show up
      isOpen: false,

      // How many items will be sent to shipment
      sendToShipmentsCount: 0,

    };

    document.querySelector('body').addEventListener('click', e => {
      if (this.mounted) {
        this.setState({isOpen: false});
      }
    });
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  onClick(e) {
    e.stopPropagation();
    this.setState({isOpen: !this.state.isOpen});
  }

  async sendToProductions(e) {
    e.preventDefault();
    let { item } = this.props;

    this.sendingModal.show();

    let response = await orderService.sendToProductions(item);

    this.sendingModal.hide();
    this.sentModal.show();

    setTimeout(() => {
      this.sentModal.hide();
    }, 2000);
    setTimeout(() => {
      this.props.parentOrdersList.fetchListData();
    }, 2300);
  }

  async holdProduction(e) {
    e.preventDefault();

    this.holdingModal.show();

    let response = await orderService.holdProduction(this.props.item);

    setTimeout(() => {
      this.holdingModal.hide();
      this.heldModal.show();
    }, 3000);
    setTimeout(() => {
      this.heldModal.hide();
    }, 5000);
    setTimeout(() => {
      this.props.parentOrdersList.fetchListData();
    }, 5300);
  }

  sendToShipments(e) {
    e.preventDefault();
    let { item } = this.props;

    console.log('this.props.item',this.props.item);

    // No need to confirm size if there is only one item
    if ( parseInt(item.quantity) === 1 || parseInt(item.quantity_production) === 1 ) {
      this.proceedToShipments();
    } else {
      this.confirmShipmentModal.show();
    }

  }

  async proceedToShipments(e) {

    if ( e && e.preventDefault ) {
      e.preventDefault();
    }

    this.confirmShipmentModal.hide();
    this.sendingToShipmentsModal.show();

    let quantity = this.sendToShipmentsCount;
    let { id: order_id } = this.props.item;
    let response = await shipmentService.use({ order_id, quantity }).save();

    this.sendingToShipmentsModal.hide();
    this.sentToShipmentsModal.show();

    setTimeout(() => {
      this.sentToShipmentsModal.hide();
    }, 2000);

    setTimeout(() => {
      this.props.parentOrdersList.fetchListData();
    }, 2300);
  }

  async markAsDone(e) {
    e.preventDefault();
    this.markingAsDoneModal.show();

    let response = await orderService.markAsDone(this.props.item);

    console.log('response',response);

    this.markingAsDoneModal.hide();
    this.markedAsDoneModal.show();

    setTimeout(() => {
      this.markedAsDoneModal.hide();
    }, 2000);

    setTimeout(() => {
      this.props.parentOrdersList.fetchListData();
    }, 2300)
  }

  confirmDelete(e) {
    e.preventDefault();
    this.confirmDeleteModal.show();
  }

  async proceedDelete() {
    this.confirmDeleteModal.hide();
    this.deletingModal.show();

    let response = await orderService.delete(this.props.item);

    this.deletingModal.hide();
    this.showDeletedModal();
  }

  showDeletedModal() {
    this.deletedModal.show();
    setTimeout(() => {
      this.deletedModal.hide();
    }, 2000);
    setTimeout(() => {
      this.props.parentOrdersList.fetchListData();
    }, 2300);
  }

  get sendToShipmentsCount() {

    if ( this.sendToShipmentsCountField ) {
      return this.sendToShipmentsCountField.value;
    }

    return 0;
  }

  get actions() {
    let actions = [];
    const { item } = this.props;
    const { id, order_number, status } = item;

    for ( let actionName of STATUS_ACTIONS[status] ) {
      let action;

      switch (actionName) {
        case 'toProductions':
          action = <a href="#" onClick={this.sendToProductions.bind(this)} className="orders-table__action" key="toProductions"><span>Send to Production</span></a>;
          break;
        case 'toShipments':
          action = <a href="#" onClick={this.sendToShipments.bind(this)} className="orders-table__action" key="toShipments"><span>Send to Shipments</span></a>;
          break;
        case 'hold':
          action = <a href="#" onClick={this.holdProduction.bind(this)} className="orders-table__action" key="hld"><span>Hold Production</span></a>;
          break;
        case 'edit':
          action = <Link to={`/orders/${id}`} className="orders-table__action" key="edit"><span>Edit order</span></Link>;
          break;
        case 'done':
          action = <a href="#" onClick={this.markAsDone.bind(this)} className="orders-table__action" key="done"><span>Mark as Done</span></a>;
          break;
        case 'delete':
          action = <a href="#" onClick={this.confirmDelete.bind(this)} className="orders-table__action" key="delete"><span>Delete this order</span></a>;
          break;
      }

      if ( !action ) continue;

      actions.push(action);
    }

    return actions;
  }

  render() {
    const { item } = this.props;
    let { id, order_number, quantity, quantity_production, status } = item;

    quantity = Number(quantity);
    quantity_production = Number(quantity_production);

    return (
      <button className='orders-table__actions' data-order-id={id} onClick={this.onClick.bind(this)}>
        <div className={`orders-table__actions__popup ${ this.state.isOpen ? 'visible' : '' }`}>
          {this.actions}
        </div>

        {/* SENDING TO PRODUCTIONS MODAL */}
        <Modal ref={ ref => this.sendingModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <div className="modal__text">Sending to Productions, please wait.</div>
        </Modal>

        {/* SENT MODAL */}
        <Modal ref={ ref => this.sentModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#${order_number}</em></div>
          <img src="/images/saved.png" alt="" className="modal__graphic"/>
          <div className="modal__text">Sent to Productions</div>
        </Modal>

        {/* HOLDING MODAL */}
        <Modal ref={ ref => this.holdingModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <div className="modal__text">Holding production, please wait.</div>
        </Modal>

        {/* HELD MODAL */}
        <Modal ref={ ref => this.heldModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <div className="modal__text">Production has been put on hold.</div>
        </Modal>

        {/* SEND TO SHIPMENTS MODAL */}
        {/* quantity should be updated to be the remaining quantity in productions */}
        {<Modal ref={ ref => this.confirmShipmentModal = ref }>
          <div className="modal__heading">Send Order <em>#{order_number}</em> to Shipment</div>
          <div className="modal__text">
            How many QTY <input type="number" name="send-to-shipment-count" maxLength="3" minLength="1" min="1" max={quantity_production} className='modal__input' defaultValue={quantity_production} ref={ el => this.sendToShipmentsCountField = el } />
            of { quantity_production }
          </div>
          <div className="modal__actions">
            <Button className="btn modal__dismiss" text="Cancel" onClick={ e => this.confirmShipmentModal.hide() } />
            <Button className="btn modal__proceed" text="Send to shipment" onClick={this.proceedToShipments.bind(this)} />
          </div>
        </Modal>}

        {/* SENDING TO SHIPMENTS MODAL */}
        <Modal ref={ ref => this.sendingToShipmentsModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <div className="modal__text">Sending to Shipments, please wait.</div>
        </Modal>

        {/* SENT TO SHIPMENTS MODAL */}
        <Modal ref={ ref => this.sentToShipmentsModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <img src="/images/saved.png" alt="" className="modal__graphic"/>
          <div className="modal__text">You sent {this.sendToShipmentsCount} out of {quantity_production}.</div>
        </Modal>

        {/* MARKING AS DONE MODAL */}
        <Modal ref={ ref => this.markingAsDoneModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <div className="modal__text">Marking as Done, please wait.</div>
        </Modal>

        {/* MARKED AS DONE MODAL */}
        <Modal ref={ ref => this.markedAsDoneModal = ref } persistent={true}>
          <div className="modal__heading">Order <em>#{order_number}</em></div>
          <div className="modal__text">Marked as Done.</div>
        </Modal>

        {/* CONFIRM DELETE MODAL */}
        <Modal ref={ ref => this.confirmDeleteModal = ref } >
          <h3 className="modal__heading">Order <em>#{order_number}</em></h3>
          <div className="modal__text">Are you sure you want to delete this order?</div>
          <div className="modal__actions">
            <Button className="btn modal__dismiss" text="No, don’t delete" onClick={ e => this.confirmDeleteModal.hide() }></Button>
            <Button className="btn modal__proceed" text="Yes, delete" onClick={this.proceedDelete.bind(this)}></Button>
          </div>
        </Modal>

        {/* DELETING STATE MODAL */}
        <Modal ref={ ref => this.deletingModal = ref } persistent={true}>
          <h3 className="modal__heading">Deleting Order <em>#{order_number}</em></h3>
          <div className="modal__text">Please wait...</div>
        </Modal>

        {/* DELETED STATE MODAL */}
        <Modal ref={ ref => this.deletedModal = ref } persistent={true}>
          <h3 className="modal__heading">Order <em>#{order_number}</em></h3>
          <div className="modal__text">Has been deleted.</div>
          <img src="/images/trash-can.png" alt="" className="modal__graphic"/>
        </Modal>

      </button>
    )
  }
}

export default withRouter(OrderActions)
