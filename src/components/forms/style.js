import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';

import styleService from '../../services/styleService';
import attributeService from '../../services/attributeService';

import ContentHeading from '../content-heading';
import SVG from '../svg';
import Form from './form';
import NumberInput from '../form-elements/number';
import Text from '../form-elements/text';
import Email from '../form-elements/email';
import Checkbox from '../form-elements/checkbox';
import DateInput from '../form-elements/date';
import Select from '../form-elements/select';
import DynamicList from '../form-elements/dynamic-list';
import ImageUpload from '../form-elements/image-upload';
import Modal from '../modal';

const CATEGORY_OPTIONS = [
  { value: 'category_1', text: 'Category 1' },
  { value: 'category_2', text: 'Category 2' },
  { value: 'category_3', text: 'Category 3' },
];

class StyleForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      style: null,
      images: [],
      steps: [],
      attributes: [],
      formErrors: {},
    }
  }

  async componentWillMount() {

    // Fetch attributes
    attributeService.all()
    .then( attributes => attributes.map( ({ id, name }) => { return {value:id, text:name} }) )
    .then( attributes => this.setState({ attributes }));

    // Fetch style data
    const styleID = this.props.computedMatch
                    ? this.props.computedMatch.params.style_id
                    : this.props.match.params.style_id;

   let style = !this.editStyle
              ? styleService.emptyStyle
              : await styleService.get(styleID);
   let { images, steps } = style;
    await this.setState({ style, images, steps });
  }

  async componentDidMount() {
    this.cacheInputBlocks();
  }

  componentDidUpdate() {
    this.cacheInputBlocks();
  }

  cacheInputBlocks() {
    this.inputBlocks = [
      this.nameInputBlock,
      this.priceInputBlock,
      this.numberInputBlock,
      this.categoryInputBlock,
      this.weightInputBlock,
      this.attributeInputBlock,
    ];
  }

  cancel() {
    alert('action: cancel');
  }

  async save() {
    let response = {};
    let formData = {...this.state.style, ...this.Form.formData};

    // Use JSON stringified values for images and steps
    formData.steps = JSON.stringify(this.stepsList.listData);
    formData.images = JSON.stringify(this.imageUpload.state.items.map( image => image.id ));

    styleService.use(formData);
    this.savingModal.show();

    try {

      if ( this.editStyle ) {
        response = await styleService.update();
      } else {
        response = await styleService.save();
      }

      let savedStyle = response.data;
      this.savingModal.hide();
      this.savedModal.show();

      setTimeout(() => {
        this.savedModal.hide();
      }, 2300);

      if ( savedStyle ) {
        setTimeout(() => {
          this.props.history.push(`/styles/${savedStyle.id}`)
          this.setState({ style: savedStyle });
        }, 2600);
      }

    // Show error modal if this any
    } catch (err) {
      this.setState({ formErrors: err.errors ? err.errors : {error: [err.message]} });
      this.savingModal.hide();
      this.formErrorModal.show();
      return;
    }
  }

  async onSubmit(e) {
    e.preventDefault();

    // Validate All
    this.inputBlocks.forEach( block => {
      block.InputBlock.validate();
    });

    let formErrors = this.Form.errors;
    let errorFieldNames = Object.keys(formErrors);

    // Save if no form error
    if ( !errorFieldNames.length ) {
      this.save();
    // Show errors on failed validation
    } else {
      await this.setStateAsync({ formErrors });
      this.formErrorModal.show();
    }
  }

  get headings() {
    const text = this.editStyle ? `Style #${this.state.style.number}` : 'New Style';
    return [
      {
        text,
        link: '#',
        active: true,
        clickable: false,
      }
    ]
  }

  get buttons() {
    return [
      {
        text: 'Cancel',
        action: this.props.history.goBack.bind(this),
      },
      {
        text: 'Save',
        active: true,
        type: 'submit',
      },
    ]
  }

  get savingModalComponent() {
    return (
      <Modal ref={ ref => this.savingModal = ref } persistent={true} >
        <div className="modal__heading">Saving Style...</div>
      </Modal>
    );
  }

  get savedModalComponent() {
    return (
      <Modal ref={ ref => this.savedModal = ref } persistent={true} >
        <div className="modal__heading">Saved!</div>
      </Modal>
    );
  }

  get formErrorModalComponent() {
    return (
      <Modal ref={ ref => this.formErrorModal = ref } >
        <div className="modal__heading">Please fix the following error(s):</div>
        <div className="modal__text">
            {Object.keys(this.state.formErrors).map( (error, key) => {
              return (
                <div key={key} >{this.state.formErrors[error][0]}</div>
              )
            })}
        </div>
      </Modal>
    );
  }

  get editStyle() {
    return ('style_id' in this.props.computedMatch.params || 'style_id' in this.props.match.params) ? true : false;

  }

  get hiddenInputs() {
    return [
      <input type="text" name='images' defaultValue={JSON.stringify(this.state.images)} ref={ ref => this.imagesInput = ref } key='images-input' hidden />,
      <input type="text" name='steps' defaultValue={JSON.stringify(this.state.steps)} ref={ ref => this.stepsInput = ref } key='steps-input' hidden />
    ]
  }

  render() {

    if ( !this.state.style ) {
      return (<div className="style-form__fetching">Fetching style...</div>)
    }

    let {
      name,
      price,
      number,
      category,
      attribute,
      weight,
    } = this.state.style;


    return (
      <Form className="style-form" ref={ref => this.Form = ref } onSubmit={this.onSubmit.bind(this)}>
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        <div className="form-fields">


          <div className="style-form__images">
            <ImageUpload items={this.state.images} ref={ ref => this.imageUpload = ref }/>
          </div>


          <div className="style-form__general">

            <Text name="name" defaultValue={name} id="name" label="Name of this style" className="style-form__name" rules='required' ref={ ref => this.nameInputBlock = ref } />
            <NumberInput name="price" defaultValue={price} id="price" label="Price" className="style-form__price" rules='required' ref={ ref => this.priceInputBlock = ref } />

            <br/>
            <NumberInput name="number" defaultValue={number} id="number" label="Style Number" className="style-form__number" rules='numeric|required' ref={ ref => this.numberInputBlock = ref } />
            <Select name="category" id="category" options={CATEGORY_OPTIONS} label="Category" className="style-form__category" ref={ ref => this.categoryInputBlock = ref } />
            <NumberInput name="weight" defaultValue={weight} id="weight" label="Weight" className="style-form__weight" ref={ ref => this.weightInputBlock = ref } />

            <br/>
            <Select name="attribute" defaultValue={attribute} id="attribute" options={this.state.attributes} label="Attribute" className="style-form__attribute" ref={ ref => this.attributeInputBlock = ref } />

            <br/>
            <DynamicList items={this.state.steps} name="steps" id="steps" label="Steps" buttonLabel="Add Step" className="style-form__steps" ref={ ref => this.stepsList = ref } />

            {this.hiddenInputs}
            {this.savingModalComponent}
            {this.savedModalComponent}
            {this.formErrorModalComponent}

          </div>

        </div>
      </Form>
    )
  }
}

export default withRouter(StyleForm)