import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';

import ColorService from '../../services/colorService';

import ContentHeading from '../content-heading';
import Form from './form';
import NumberInput from '../form-elements/number';
import Text from '../form-elements/text';
import ColorPicker from '../form-elements/colorpicker';
import Modal from '../modal';

const COLOR_RULES = [
  'required',
  'regex:/^#([0-9a-f]{3}|[0-9a-f]{6})$/', // HEX
];

class ColorForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      color: null,
      formErrors: {},
    };
  }

  async save() {
    let response = {};
    let formData = {...this.state.color, ...this.Form.formData};

    ColorService.use(formData);
    this.savingModal.show();

    try {

      if ( this.editColor ) {
        response = await ColorService.update();
      } else {
        response = await ColorService.save();
      }

      let savedColor = response.data;
      this.savingModal.hide();
      this.savedModal.show();

      setTimeout(() => {
        this.savedModal.hide();
      }, 2300);

      if ( savedColor ) {
        setTimeout(() => {
          this.props.history.push(`/colors/${savedColor.id}`)
          this.setState({ color: savedColor });
        }, 2600);
      }

    // Show error modal if this any
    } catch (err) {
      this.setState({ formErrors: err.errors ? err.errors : {error: [err.message]} });
      this.savingModal.hide();
      this.formErrorModal.show();
      return;
    }
  }

  async onSubmit(e) {
    e.preventDefault();

    // Validate all
    this.inputBlocks.forEach( block => {
      block.InputBlock.validate();
    });

    let formErrors = this.Form.errors;
    let errorFieldNames = Object.keys(formErrors);

    // Save if no form error
    if ( !errorFieldNames.length ) {
      this.save();
    // Show errors on failed validation
    } else {
      await this.setStateAsync({ formErrors });
      this.formErrorModal.show();
    }
  }

  get headings() {
    const text = !this.editColor ? 'New Color' : `Color #${this.state.color.color_number}`;
    return [
      {
        text,
        link: '#',
        active: true,
        clickable: false,
      }
    ]
  }

  get buttons() {
    return [
      {
        text: 'Cancel',
        action: this.props.history.goBack.bind(this),
      },
      {
        text: 'Save',
        active: true,
        type: 'submit',
      },
    ]
  }

  get editColor() {
    return ('color_id' in this.props.match.params || 'color_id' in this.props.computedMatch.params ) ? true : false;
  }

  get savingModalComponent() {
    return (
      <Modal ref={ ref => this.savingModal = ref } persistent={true} >
        <div className="modal__heading">Saving Color...</div>
      </Modal>
    );
  }

  get savedModalComponent() {
    return (
      <Modal ref={ ref => this.savedModal = ref } persistent={true} >
        <div className="modal__heading">Saved!</div>
      </Modal>
    );
  }

  get formErrorModalComponent() {
    return (
      <Modal ref={ ref => this.formErrorModal = ref } >
        <div className="modal__heading">Please fix the following error(s):</div>
        <div className="modal__text">
            {Object.keys(this.state.formErrors).map( (error, key) => {
              return (
                <div key={key} >{this.state.formErrors[error][0]}</div>
              )
            })}
        </div>
      </Modal>
    );
  }

  get matchedIdParam() {
    if ( 'color_id' in this.props.computedMatch.params ) {
      return this.props.computedMatch.params.color_id;
    } else {
      return this.props.match.params.color_id;
    }
  }

  async componentDidMount() {

    let color = !this.editColor
                ? ColorService.emptyColor
                : await ColorService.get( this.matchedIdParam );

    await this.setState({ color });

    this.inputBlocks = [
      this.colorNumberInputBlock,
      this.colorNameInputBlock,
      this.colorValueInputBlock,
    ];
  }

  render() {

    if ( !this.state.color ) {
      return (<div className="color-form__fetching">Fetching Color...</div>)
    }

    let { color_number, color_name, color_value } = this.state.color;

    return (
      <Form className="color-form" onSubmit={this.onSubmit.bind(this)} ref={ ref => this.Form = ref }>
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        <div className="form-fields">
          <NumberInput defaultValue={color_number} name="color_number" label="Color Number" id="color_number" rules="required|numeric" ref={ ref => this.colorNumberInputBlock = ref }/>
          <Text defaultValue={color_name} name="color_name" label="Color Name/Title" id="color_text" rules="required" ref={ ref => this.colorNameInputBlock = ref }/>
          <ColorPicker defaultValue={color_value} name="color_value" value="#" label="Color Picker" id="color_value" rules={COLOR_RULES} ref={ ref => this.colorValueInputBlock = ref }/>
        </div>

        {this.savingModalComponent}
        {this.savedModalComponent}
        {this.formErrorModalComponent}
      </Form>
    )
  }
}

export default withRouter(ColorForm)