import React, {Component, Fragment} from 'react';
import { createPortal } from 'react-dom';
import { withRouter } from 'react-router-dom';

// Mock data
import mockCustomers from '../../mock/customers';

import orderService from '../../services/orderService';
import customerService from '../../services/customerService';
import styleService from '../../services/styleService';
import colorService from '../../services/colorService';

import ContentHeading from '../content-heading';
import SVG from '../svg';
import Form from './form';
import NumberInput from '../form-elements/number';
import Text from '../form-elements/text';
import Email from '../form-elements/email';
import Checkbox from '../form-elements/checkbox';
import DateInput from '../form-elements/date';
import Select from '../form-elements/select';
import Modal from '../modal';

const body = document.querySelector('body')

// [ new, processing, printing, shipping, shipped ]
const STATUS_OPTIONS = [
  { value: 'new', text: 'New Order' },
  { value: 'production', text: 'In Productions' },
  { value: 'hold', text: 'Hold Productions' },
  { value: 'shipment', text: 'Shipment' },
  { value: 'done', text: 'Done' },
  { value: 'cancelled', text: 'Cancelled' },
];

const CLASSNAMES = {
  results: 'results',
  busy: 'busy',
}

class OrderForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customerResults: [],
      styleOptions: [],
      colorOptions: [],
      order: null,
      customer: null,
      style: null,
      color: null,
      formAction: 'new',
      formErrors: [],
      requestError: null,
    };
  }

  async componentWillMount() {

    styleService.all().then( styles => {
      let styleOptions = styles.map( ({id, name}) => {
        return { value: id, text: name };
      });
      this.setState({ styleOptions: [{}, ...styleOptions] });
    });
    colorService.all().then( colors => {
      let colorOptions = colors.map( ({id, color_name}) => {
        return { value: id, text: color_name };
      });
      this.setState({ colorOptions: [{}, ...colorOptions] });
    });

    let order = !this.editOrder
                ? orderService.emptyOrder
                : await orderService.get( this.matchedIdParam );

    this.setState({ order, customer: order.customer, style: order.style });
  }

  async componentDidMount() {
    this.cacheInputBlocks();
  }

  cacheInputBlocks() {
    this.inputBlocks = [
      this.SearchInput,
      this.orderNumberBlock,
      this.accountNumberBlock,
      this.addressBlock,
      this.cityBlock,
      this.stateBlock,
      this.countryBlock,
      this.phoneBlock,
      this.emailBlock,
      this.orderDateBlock,
      this.cancelDateBlock,
      this.completionDateBlock,
      this.quantityBlock,
      this.styleBlock,
      this.colorBlock,
      this.customerPONumberBlock,
      this.shipCodeBlock,
      this.termsBlock,
      this.statusBlock,
    ];
  }

  /**
   * Performs search on keyup
   */
  onKeyUp(e) {
    let value = e.target.value;

    // Cancel any existing search operation
    this.currentSearch = false;

    this.toggleResultsState(false);
    this.toggleBusyState(false);

    if ( !value ) return this.setState({ customerResults: [] });

    // Perform search
    this.performSearch(value);
  }

  async save() {
    let response = {};
    orderService.use(this.formData);

    this.savingModal.show();

    try {

      if ( this.editOrder ) {
        response = await orderService.update();
      } else {
        response = await orderService.save();
      }

      let savedOrder = response.data;
      this.savingModal.hide();
      this.savedModal.show();

      setTimeout(() => {
        this.savedModal.hide();

      }, 2300);

      if ( savedOrder ) {
        setTimeout(() => {
          this.props.history.push(`/orders/${savedOrder.id}`)
          this.setState({ order: savedOrder });
        }, 2600);
      }

    } catch (err) {
      err = await err.json();
      this.setState({ formErrors: err.errors });
      this.savingModal.hide();
      this.formErrorModal.show();
      console.warn(err);
      return;
    }

  }

  onStatusSelect(e) {
    console.log('e.target',e.target);
  }

  toggleBusyState( bool ) {
    let params = [CLASSNAMES.busy];
    params = params.concat(typeof bool === 'undefined' ? [] : [ bool ]);
    this.SearchInput.InputBlock.element.classList.toggle(...params);
  }

  toggleResultsState( bool ) {
    let params = [CLASSNAMES.results];
    params = params.concat(typeof bool === 'undefined' ? [] : [ bool ]);
    this.SearchInput.InputBlock.element.classList.toggle(...params);
  }

  async performSearch( name = '' ) {
    this.toggleBusyState(true);

    // Attach an identifier of this search operation to the component
    const currentTimestamp = Date.now();
    this.latestTimestamp = currentTimestamp;

    let result = await customerService.search(name);

    // Proceed if this is still the latest request fired
    if ( this.latestTimestamp === currentTimestamp ) {
      this.showResults(result); // mockCustomers should be replaced with actual data
      this.toggleBusyState(false);
    }

  }

  showResults( customerResults ) {
    this.setState({ customerResults });
    this.toggleResultsState(true);
  }

  selectCustomer(customer) {
    this.toggleResultsState(false);
    this.SearchInput.InputBlock.input.value = customer.name;
    this.customerIdInput.value = customer.id;
    this.setState({ customer });
  }

  async onSubmit(e) {
    e.preventDefault();

    this.cacheInputBlocks();

    // Validate all
    this.inputBlocks.forEach( block => {
      block.InputBlock.validate();
    });

    let formErrors = this.Form.errors;
    let errorFieldNames = Object.keys(formErrors);

    // Save if no form error
    if ( !errorFieldNames.length ) {
      this.save();
    // Show errors on failed validation
    } else {
      await this.setStateAsync({ formErrors });
      this.formErrorModal.show();
    }
  }

  get formData() {
    console.log('this.Form.formData',this.Form.formData);
    return this.Form.formData;
  }

  /**
   * Determine new or edit form type according to params
   *
   */
  get editOrder() {
    return ('order_id' in this.props.match.params || 'order_id' in this.props.computedMatch.params ) ? true : false;
  }

  get headings() {
    const text = !this.editOrder ? 'New Order' : `Order #${this.state.order.order_number}`;
    return [
      {
        text,
        link: '#',
        active: true,
        clickable: false,
      }
    ]
  }

  get buttons() {
    return [
      {
        text: 'Cancel',
        action: this.props.history.goBack.bind(this),
      },
      {
        text: 'Save',
        active: true,
        type: 'submit',
      },
    ]
  }

  get searchResultItems() {
    return this.state.customerResults.map( (customer, key) => (
      <div className="order-form__search__item" key={key} onClick={e => this.selectCustomer(customer)} >{customer.name}</div>
    ));
  }

  get searchPopup() {
    return (
      <Fragment>
        <div className="order-form__search__popup">
          <div className="order-form__search__result">
            {this.searchResultItems}
          </div>
          <a href="/customers/new" className="btn order-form__search__cta">Add New Customer</a>
        </div>
        <SVG name='loading' className='order-form__search__loading' />
      </Fragment>
    )
  }

  get savingModalComponent() {
    return (
      <Modal ref={ ref => this.savingModal = ref } persistent={true} >
        <div className="modal__heading">Saving Order...</div>
      </Modal>
    );
  }

  get savedModalComponent() {
    return (
      <Modal ref={ ref => this.savedModal = ref } persistent={true} >
        <div className="modal__heading">Saved!</div>
      </Modal>
    );
  }

  get formErrorComponent() {
    return (
      <Modal ref={ ref => this.formErrorModal = ref } >
        <div className="modal__heading">Please fix the following errors:</div>
        <div className="modal__text">
            {Object.keys(this.state.formErrors).map( (error, key) => {
              return (
                <div key={key} >{this.state.formErrors[error][0]}</div>
              )
            })}
        </div>
      </Modal>
    );
  }

  get matchedIdParam() {
    if ( 'order_id' in this.props.computedMatch.params ) {
      return this.props.computedMatch.params.order_id;
    } else {
      return this.props.match.params.order_id;
    }
  }

  render() {

    if ( !this.state.order ) {
      return (<div className="order-form__fetching">Fetching order...</div>)
    }

    const { customer, style, order } = this.state;
    const shipping = parseInt(customer.shipping_address_same) ? {
                      shipping_address : customer.address,
                      shipping_city : customer.city,
                      shipping_state : customer.state,
                      shipping_country : customer.country,
                    } : {
                      shipping_address : customer.shipping_address,
                      shipping_city : customer.shipping_city,
                      shipping_state : customer.shipping_state,
                      shipping_country : customer.shipping_country,
                    } ;

    return (
      <Form className="order-form" ref={ ref => this.Form = ref} onSubmit={this.onSubmit.bind(this)}>
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        <div className="form-fields">

          <Text label='Customer' rules='required' name='customer' icon='search' defaultValue={customer.name} className='order-input-block__name order-form__search' id='name' onKeyUp={this.onKeyUp.bind(this)} ref={ ref => this.SearchInput = ref }>
            {this.searchPopup}
          </Text>
          <input type="number" name='customer_id' defaultValue={customer.id} hidden ref={el => this.customerIdInput = el} />
          <input type="number" name='id' value={order.id} hidden />

          <NumberInput name='order_number' defaultValue={order.order_number} label='Order Number' className='order-input-block__account_number' id='order_number' rules='numeric|required' ref={ ref => this.orderNumberBlock = ref } />
          <NumberInput name='account_number' value={customer.account_number} label='Account Number' className='order-input-block__account_number' id='account_number' rules='required' ref={ ref => this.accountNumberBlock = ref } />
          <br/>

          <Text name='address' readOnly value={shipping.shipping_address} label='Address' className='order-input-block__address' id='address' ref={ ref => this.addressBlock = ref } />
          <Text name='city' readOnly value={shipping.shipping_city} label='City' className='order-input-block__city' id='city' ref={ ref => this.cityBlock = ref } />
          <Text name='state' readOnly value={shipping.shipping_state} label='State' className='order-input-block__state' id='state' ref={ ref => this.stateBlock = ref } />
          <Text name='country' readOnly value={shipping.shipping_country} label='Country' className='order-input-block__country' id='country' ref={ ref => this.countryBlock = ref } />
          <br/>

          <Text name='phone' readOnly value={customer.phone} label='Phone Number' className='order-input-block__phone' id='phone' ref={ref => this.phoneBlock = ref }  />
          <Email name='email' readOnly value={customer.email} label='Email' className='order-input-block__email' id='email' ref={ref => this.emailBlock = ref }  />
          <br/>

          <DateInput name='order_date' defaultValue={order.order_date} label='Order Date' className='order-input-block__order_date' id='order_date' rules='required' ref={ref => this.orderDateBlock = ref } />
          <DateInput name='cancel_date' defaultValue={order.cancel_date} label='Cancel Date' className='order-input-block__cancel_date' id='cancel_date' ref={ref => this.cancelDateBlock = ref } />
          <DateInput name='completion_date' defaultValue={order.completion_date} label='Completion Date' className='order-input-block__completion_date' id='completion_date' ref={ref => this.completionDateBlock = ref } />
          <br/>

          <NumberInput name='quantity' defaultValue={order.quantity} label='Quantity' className='order-input-block__quantity' id='quantity' rules='required' ref={ref => this.quantityBlock = ref}/>
          <Select name="style_id" defaultValue={order.style_id} label="Style" options={this.state.styleOptions} className='order-input-block__style' id='style' rules='required' ref={ref => this.styleBlock = ref}/>
          <Select name="color_id" defaultValue={order.color_id} label="Color" options={this.state.colorOptions} className='order-input-block__color' id='color' rules='required' ref={ref => this.colorBlock = ref}/>
          <br/>

          <NumberInput name='customer_po_number' defaultValue={order.customer_po_number} label='Customer PO Number' className='order-input-block__customer_po_number' id='customer_po_number' ref={ref => this.customerPONumberBlock = ref } />
          <Text name='ship_code' defaultValue={order.ship_code} label='Ship Code' className='order-input-block__ship_code' id='ship_code' ref={ref => this.shipCodeBlock = ref } />
          <Text name='terms' defaultValue={order.terms} label='Terms' className='order-input-block__terms' id='terms' ref={ref => this.termsBlock = ref } />
          <br/>

          <Select name="status" defaultValue={order.status} label="Status" icon='flag' options={STATUS_OPTIONS} onChange={this.onStatusSelect.bind(this)} className='order-input-block__status' id='status' ref={ref => this.statusBlock = ref }/>

        </div>

        {createPortal( this.savingModalComponent, body)}
        {createPortal( this.savedModalComponent, body)}
        {createPortal( this.formErrorComponent, body)}

      </Form>
    )
  }
}

export default withRouter(OrderForm)