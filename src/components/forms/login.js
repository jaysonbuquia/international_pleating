import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';

import AuthService from '../../services/authService';

import Form from './form';
import Email from '../form-elements/email';
import Password from '../form-elements/password';
import Button from '../button';
import SVG from '../svg';

class LoginForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      formError: null,
      showForm: false,
    }
  }

  componentWillMount() {
    if ( AuthService.isLoggedIn ) {
      window.location.href = '/';
    } else {
      this.setState({ showForm: true });
    }
  }

  async onSubmit(e) {
    e.preventDefault();

    this.Form.element.classList.add('login-form--busy');

    try {
      await AuthService.login(this.Form.formData);
    } catch (response) {
      console.log('response',response);
      const err = await response.json();
      console.log('err',err);
      this.setState({ formError: err.message });
      this.Form.element.classList.remove('login-form--busy');
      console.error(err);
    }
  }

  onChange() {
    let isFormValid = this.Form.isValid;
    this.Form.element.classList.toggle('form--invalid', !isFormValid);
    this.Form.element.classList.toggle('form--valid', isFormValid);

    // Remove error notifications
    this.setState({ formError: null });

    if ( isFormValid ) {
      this.Form.submit.removeAttribute('disabled');
    } else {
      this.Form.submit.setAttribute('disabled','disabled');
    }

  }

  onFieldUpdate() {
    this.Form.element.classList.toggle('form--invalid', this.Form.hasErrors);
    this.Form.element.classList.toggle('form--valid', this.Form.isValid);
  }

  get formError() {
    if ( this.state.formError ) {
      return (<div className="login-form__error">{this.state.formError}</div>)
    }
    return null
  }

  render() {

    if ( !this.state.showForm ) {
      return null;
    }

    return (
      <Form className="login-form" ref={ref => this.Form = ref } action={this.action} onChange={this.onChange.bind(this)} onSubmit={this.onSubmit.bind(this)} >
        <h1 className="login-form__heading">International Pleating</h1>
        <div className="login-form__fields">
          {this.formError}
          <Email name="email" animatedLabel={true} icon='user' rules="required|email" />
          <Password name="password" animatedLabel={true} rules="required|min:6" />
          <div className="login-form__action">
            <div className="login-form__loading"><SVG name='loading' /></div>
            <Button type="submit" text="LOG IN" disabled />
          </div>
          {this.buttonOrLoader}
        </div>
      </Form>
    )
  }

}

export default withRouter(LoginForm);


// onFieldUpdate={this.onFieldUpdate.bind(this)}