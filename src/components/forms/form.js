import React, {Component} from 'react';
import Validator from 'Validator';
import cookie from 'react-cookies';

const CSRF_TOKEN = document.querySelector('[name=csrf-token]').content;

class Form extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {

  }

  onSubmit(e) {
    e.preventDefault();
    // this.validateAll();

    if ( this.props.onSubmit ) {
      this.props.onSubmit(e);
    }
  }

  get rules() {
    let fieldRules = this.element.querySelectorAll('[data-rules]');
    let rules = {};

    this.submit = this.element.querySelector('[type=submit]');

    if ( fieldRules.length ) {
      for ( let field of fieldRules ) {
        rules[field.name] = field.getAttribute('data-rules');
      }
    };

    return rules;
  }

  get token() {
    return CSRF_TOKEN;
  }

  get hasErrors() {
    return this.element.querySelectorAll('.error').length ? true : false;
  }

  get validation() {
    return Validator.make(this.formData, this.rules, this.errorMessages, this.fieldNames);
  }

  get errors() {
    let validation = this.validation;
    if ( validation.fails() ) {
      return validation.getErrors();
    }
    return {};
  }

  get isValid() {
    let validation = Validator.make(this.formData, this.rules, this.errorMessages, this.fieldNames);
    return validation.passes();
  }

  get formData() {
    let _formData = {};
    let fields = this.element.querySelectorAll('[name]');

    for ( let field of fields ) {
      _formData[field.name] = field.value;
    }

    if ( typeof this.formDataTransform === 'function' ) {
      _formData = this.formDataTransform(_formData);
    }

    return _formData;
  }

  get formDataRaw() {
    let formDataRaw = new FormData(this.element);
    // formDataRaw.append('_token', this.token);
    return formDataRaw;
  }

  render() {
    let { children, ...props } = this.props;
    return (
      <form {...props} ref={ref=> this.element = ref} onSubmit={this.onSubmit.bind(this)}>
        {children}
      </form>
    )
  }

}

export default Form;