import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import Modal from '../modal';

import customerService from '../../services/customerService';

import ContentHeading from '../content-heading';
import Form from './form';
import NumberInput from '../form-elements/number';
import Text from '../form-elements/text';
import Email from '../form-elements/email';
import Checkbox from '../form-elements/checkbox';

class CustomerForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customer: null,
      formErrors: {},
    };
  }

  async componentDidMount() {

    const customerID = 'computedMatch' in this.props
                        ? this.props.computedMatch.params.customer_id
                        : this.props.match.params.customer_id;

    let customer = !this.editCustomer
                ? customerService.emptyCustomer
                : await customerService.get( customerID );
    await this.setState({ customer });

    // Add form hooks
    this.Form.errorMessages = {
      required: ':attr is required.',
      email: ':attr should be a valid email.',
      numeric: ':attr should be a number.',
    };
    this.Form.fieldNames = {
      name: 'Customer name',
      account_number: 'Account number',
      address: 'Address',
      city: 'City',
      state: 'State',
      country: 'Country',
      phone: 'Phone',
      email: 'Email',
      shipping_number: 'Shipping number',
      credit_limit: 'Credit limit',
      shipping_address: 'Shipping Address',
      shipping_city: 'Shipping City',
      shipping_state: 'Shipping State',
      shipping_country: 'Shipping Country',
    };

    this.inputBlocks = [
      this.nameBlock,
      this.accountNumberBlock,
      this.addressBlock,
      this.cityBlock,
      this.stateBlock,
      this.countryBlock,
      this.phoneBlock,
      this.emailBlock,
      this.termsCodeBlock,
      this.shipCodeBlock,
      this.shippingNumberBlock,
      this.creditLimitBlock,
      this.notesBlock,
      this.shippingAddressSameBlock,
      this.shippingAddressBlock,
      this.shippingCityBlock,
      this.shippingStateBlock,
      this.shippingCountryBlock,
    ];
  }

  async save() {
    let response = {};
    let formData = {...this.state.customer, ...this.Form.formData};

    customerService.use(formData);
    this.savingModal.show();

    try {

      if ( this.editCustomer ) {
        response = await customerService.update();
      } else {
        response = await customerService.save();
      }

      let savedCustomer = response.data;
      this.savingModal.hide();
      this.savedModal.show();

      setTimeout(() => {
        this.savedModal.hide();
      }, 2300);

      if ( savedCustomer ) {
        setTimeout(() => {
          this.setState({ customer: savedCustomer });
          this.props.history.push(`/customers/${savedCustomer.id}`);
        }, 2600);
      }

    // Show error modal if this any
    } catch (err) {
      err = await err.json();
      this.setState({ formErrors: err.errors ? err.errors : {error: [err.message]} });
      this.savingModal.hide();
      this.formErrorModal.show();
      return;
    }

  }

  toggleShippingFields(e) {
    let isChecked = e.target.checked;
    // Toggle disabled state
    this.shippingAddressBlock.InputBlock.input.disabled = isChecked;
    this.shippingCityBlock.InputBlock.input.disabled = isChecked;
    this.shippingStateBlock.InputBlock.input.disabled = isChecked;
    this.shippingCountryBlock.InputBlock.input.disabled = isChecked;

    // Autocomplete/Remove shipping fields values
    if ( isChecked ) {
      this.shippingAddressBlock.InputBlock.input.value = this.addressBlock.InputBlock.input.value;
      this.shippingCityBlock.InputBlock.input.value = this.cityBlock.InputBlock.input.value;
      this.shippingStateBlock.InputBlock.input.value = this.stateBlock.InputBlock.input.value;
      this.shippingCountryBlock.InputBlock.input.value = this.countryBlock.InputBlock.input.value;
    } else {
      this.shippingAddressBlock.InputBlock.input.value = '';
      this.shippingCityBlock.InputBlock.input.value = '';
      this.shippingStateBlock.InputBlock.input.value = '';
      this.shippingCountryBlock.InputBlock.input.value = '';
    }

  }

  async onSubmit(e) {
    e.preventDefault();

    // Validate all
    this.inputBlocks.forEach( block => {
      block.InputBlock.validate();
    });

    let formErrors = this.Form.errors;
    let errorFieldNames = Object.keys(formErrors);

    // Save if no form error
    if ( !errorFieldNames.length ) {
      this.save();
    // Show errors on failed validation
    } else {
      await this.setStateAsync({ formErrors });
      this.formErrorModal.show();
    }
  }

  get savingModalComponent() {
    return (
      <Modal ref={ ref => this.savingModal = ref } persistent={true} >
        <div className="modal__heading">Saving Customer...</div>
      </Modal>
    );
  }

  get savedModalComponent() {
    return (
      <Modal ref={ ref => this.savedModal = ref } persistent={true} >
        <div className="modal__heading">Saved!</div>
      </Modal>
    );
  }

  get formErrorModalComponent() {
    return (
      <Modal ref={ ref => this.formErrorModal = ref } >
        <div className="modal__heading">Please fix the following error(s):</div>
        <div className="modal__text">
            {Object.keys(this.state.formErrors).map( (error, key) => {
              return (
                <div key={key} >{this.state.formErrors[error][0]}</div>
              )
            })}
        </div>
      </Modal>
    );
  }

  get headings() {
    const text = this.editCustomer ? this.state.customer.name : 'New Customer';
    return [
      {
        text,
        link: '#',
        active: true,
        clickable: false,
      }
    ]
  }

  get buttons() {
    return [
      {
        text: 'Cancel',
        action: this.props.history.goBack.bind(this),
      },
      {
        text: 'Save',
        active: true,
        type: 'submit',
      },
    ]
  }

  get editCustomer() {
    return ('customer_id' in this.props.match.params || 'customer_id' in this.props.computedMatch.params) ? true : false;
  }

  render() {

    if ( !this.state.customer ) {
      return (<div className="customer-form__fetching">Fetching customer...</div>)
    }


    let {
      name,
      account_number,
      address,
      city,
      state,
      country,
      contact,
      phone,
      email,
      terms_code,
      ship_code,
      shipping_number,
      credit_limit,
      notes,
      shipping_address_same,
      shipping_address,
      shipping_city,
      shipping_state,
      shipping_country
    } = this.state.customer;

    return (

      <Form className="customer-form" ref={ ref => this.Form = ref } onSubmit={this.onSubmit.bind(this)}>
        <ContentHeading headings={this.headings} buttons={this.buttons} />
        <div className="form-fields">

          <Text name='name' defaultValue={name} label='Customer' className='customer-input-block__name' id='name' rules='required' ref={ ref => this.nameBlock = ref} />
          <NumberInput name='account_number' defaultValue={account_number} label='Account Number' className='customer-input-block__account_number' id='account_number' rules='numeric|required' ref={ ref => this.accountNumberBlock = ref} />
          <br/>

          <Text name='address' rules='required' defaultValue={address} label='Address' className='customer-input-block__address' id='address' ref={ ref => this.addressBlock = ref} />
          <Text name='city' rules='required' defaultValue={city} label='City' className='customer-input-block__city' id='city' ref={ ref => this.cityBlock = ref} />
          <Text name='state' rules='required' defaultValue={state} label='State' className='customer-input-block__state' id='state' ref={ ref => this.stateBlock = ref} />
          <Text name='country' rules='required' defaultValue={country} label='Country' className='customer-input-block__country' id='country' ref={ ref => this.countryBlock = ref} />
          <br/>

          <Text name='contact' defaultValue={contact} rules='required' label='Contact Name' className='customer-input-block__contact' id='contact' ref={ ref => this.contactBlock = ref} />
          <Text name='phone' defaultValue={phone} rules='required' label='Phone Number' className='customer-input-block__phone' id='phone' ref={ ref => this.phoneBlock = ref} />
          <Email name='email' defaultValue={email} rules='email|required' label='Email' className='customer-input-block__email' id='email' ref={ ref => this.emailBlock = ref} />
          <br/>

          <Text name='terms_code' defaultValue={terms_code} label='Terms Code' className='customer-input-block__terms_code' id='terms_code' ref={ ref => this.termsCodeBlock = ref} />
          <Text name='ship_code' defaultValue={ship_code} label='Ship Code' className='customer-input-block__ship_code' id='ship_code' ref={ ref => this.shipCodeBlock = ref} />
          <NumberInput name='shipping_number' defaultValue={shipping_number} label='Shipping Number' className='customer-input-block__shipping_number' id='shipping_number' rules='numeric' ref={ ref => this.shippingNumberBlock = ref} />
          <br/>

          <NumberInput name='credit_limit' defaultValue={credit_limit} label='Credit Limit' className='customer-input-block__credit_limit' id='credit_limit' ref={ ref => this.creditLimitBlock = ref} />
          <Text name='notes' defaultValue={notes} label='Notes' icon='flag' className='customer-input-block__notes' id='notes' ref={ ref => this.notesBlock = ref} />
          <br/>

          <Checkbox name='shipping_address_same' checked={parseInt(shipping_address_same) ? true : false } label='Shipping address is the same as above' className='customer-input-block__shipping_address_same' id='shipping_address_same' ref={ ref => this.shippingAddressSameBlock = ref} onChange={this.toggleShippingFields.bind(this)} />
          <br/>

          <Text name='shipping_address' defaultValue={shipping_address} rules='required' id='shipping_address' className='customer-input-block__shipping_address' label='Shipping Address' ref={ ref => this.shippingAddressBlock = ref} />
          <Text name='shipping_city' defaultValue={shipping_city} rules='required' id='shipping_city' className='customer-input-block__shipping_city' label='City' ref={ ref => this.shippingCityBlock = ref} />
          <Text name='shipping_state' defaultValue={shipping_state} rules='required' id='shipping_state' className='customer-input-block__shipping_state' label='State' ref={ ref => this.shippingStateBlock = ref} />
          <Text name='shipping_country' defaultValue={shipping_country} rules='required' id='shipping_country' className='customer-input-block__shipping_country' label='Country' ref={ ref => this.shippingCountryBlock = ref} />
        </div>

        {this.savingModalComponent}
        {this.savedModalComponent}
        {this.formErrorModalComponent}
      </Form>
    )
  }
}

export default withRouter(CustomerForm)