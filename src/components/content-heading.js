import React, {Component, Fragment} from 'react';
import {withRouter, Link} from 'react-router-dom';

import Button from './button';

class ContentHeading extends Component {

  constructor(props) {
    super(props);
  }

  // Generates content heading titles
  get headings() {
    let { pathname } = this.props.location;

    return this.props.headings.map( ({text, link, className = null, active = false, clickable = true}, key) => {
      return <Link
        to={pathname == link ? '#' : link}
        key={key}
        onClick={ e => clickable ? true : false }
        className={`content-heading__headings__item ${className ? className : ''} ${pathname == link || active ? 'active' : '' }`}>
        {text}
      </Link>
    });
  }

  // Generates content heading actions
  get actions() {
    return this.props.buttons.map( ({ text, action = null, active = false, link = null, ...props }, key) => {
     return link
            ? <Link className={`btn content-heading__actions__button ${active ? 'active' :''}`} to={link} onClick={action} key={key} {...props} >{text}</Link>
            : <Button className={`btn content-heading__actions__button ${active ? 'active' :''}`} onClick={action} text={text} key={key} {...props} />
    });
  }

  render() {
    return (
      <div className="content-heading">
        <div className="content-heading__headings">{this.headings}</div>
        <div className="content-heading__actions">{this.actions}</div>
      </div>
    )
  }
}

export default withRouter(ContentHeading);
