import React, {Component, Fragment} from 'react';
import { subscribe } from 'pubsub-js';

import { invoicePrintDataSet, invoicePrintPreparing, invoicePrintReady } from '../services/topicsService';
import { monthNames } from '../services/dateService';

import Modal from './modal';
import SVG from './svg';

class PrintInvoice extends Component {

  constructor(props) {
    super(props);

    this.state = {

      // Will contain the order info
      invoiceOrders: null,
    }
  }


  componentDidMount() {
    subscribe(invoicePrintDataSet, (action, invoiceOrders) => this.setState({ invoiceOrders }));
  }


  formatDate(date) {
    if ( !date ) {
      return '';
    }

    const dateTime = new Date(date);
    const monthName = monthNames[dateTime.getMonth()];
    const day = dateTime.getDay();
    const year = dateTime.getFullYear();
    return `${monthName} ${day}, ${year}`;
  }

  formatPrice(price) {
    price = parseFloat(price);
    price = price.toFixed(2);

    return price.replace('.',',');
  }

  get orderDetails() {
    let total = 0;
    let orderRows =  this.state.invoiceOrders.map( (order, index) => {

      const { order_number, style, color, quantity } = order;
      const orderTotal = parseFloat(style.price) * parseInt(quantity);
      total += orderTotal;

      return (
        <tr className="print-invoice__order-row" key={index}>
          <td>{order_number}</td>
          <td>{color.color_name} Cut Number {style.number}</td>
          <td>{quantity}</td>
          <td>{this.formatPrice(style.price)} USD</td>
          <td>{this.formatPrice(orderTotal)} USD</td>
        </tr>
      )
    });

    let totalRow = (
      <tr className="print-invoice__total-row" key="total">
        <td></td><td></td><td className="print-invoice__total-row__descriptor">Total</td><td></td><td className="print-invoice__total-row__amount">{this.formatPrice(total)} USD</td>
      </tr>
    )
    return [...orderRows, totalRow];
  }


  render() {

    if ( !this.state.invoiceOrders || !this.state.invoiceOrders.length ) {
      return null;
    }

    const {
      order_number,
      customer,
      order_date,
      completion_date,
      address,
      city,
      state,
      country
    } = this.state.invoiceOrders[0];

    return (
      <div className="print-page print-invoice">

        {/* HEADER */}
          <div className="print-invoice__header">
            <div className="print-invoice__header-top">
              <div className="print-invoice__address">
                INTERNATIONAL PLEATING <br/>
                327 W 36th St #400 <br/>
                New York, NY 10018, USA
              </div>
              <div className="print-invoice__contact">
                <p><SVG name='envelope'/>george@internationalpleating.com</p>
                <p><SVG name='phone'/>+1 646-580-9766</p>
              </div>
            </div>

            <div className="print-invoice__header-bottom">
              <div className="print-invoice__recipient">
                <h3>Recipient</h3>
                <p>
                  {customer.contact} <br/>
                  {customer.address} <br/>
                  {customer.city}, {customer.state} <br/>
                  {customer.country}
                </p>
                <p>
                  <span><SVG name='envelope'/>{customer.email}</span> <br/>
                  <span><SVG name='phone'/>{customer.phone}</span>
                </p>
              </div>
              <div className="print-invoice__info">
                <h1>Invoice</h1>
                <h3>Invoice No.</h3>
                <p>123</p>

                {
                  completion_date ?
                  <Fragment>
                    <h3>Invoice Date</h3>
                    <p>{this.formatDate(completion_date)}</p>
                  </Fragment>
                  : null
                }

              </div>
            </div>
          </div>
        {/* END HEADER */}

        {/* BODY */}
          <div className="print-invoice__body">

            <table className="print-invoice__table">
              <thead>
                <tr>
                  <td>Order #</td>
                  <td>Style</td>
                  <td>Qty</td>
                  <td>Rate</td>
                  <td>Amount</td>
                </tr>
              </thead>
              <tbody>
                {this.orderDetails}
                {}
              </tbody>
            </table>

            <div className="print-invoice__notes">
              <h3>Notes</h3>
              <p>
                All amounts are in dollars. Please make the payment within 15 days from the issue of date of this invoice. Tax is not charged on the basis of paragraph 1 of Article 94 of the Value Added Tax Act (I am not liable for VAT).
                <br/><br/>
                Thank you for you confidence in my work. <br/>
                Signature
              </p>
            </div>

          </div>
        {/* END BODY */}

      </div>
    )

  }
}

export default PrintInvoice