import React, {Component} from 'react';
import {withRouter, Link} from 'react-router-dom';

class UserBlock extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { first_name, last_name, role, photo } = this.props.user;

    return (
      <div className="user-block">
        <div className="user-block__text">
          <div className="user-block__username">{first_name}</div>
          <div className="user-block__role">{role}</div>
        </div>
        <div className="user-block__photo">
          { photo ? <img src={photo.src} alt=""/> : null }
        </div>
        <Link to="/logout" className="user-block__logout">Log out</Link>
      </div>
    )
  }

}

export default withRouter(UserBlock);