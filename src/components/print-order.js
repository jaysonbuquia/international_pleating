import React, {Component, Fragment} from 'react';
import { subscribe } from 'pubsub-js';

import { orderPrintDataSet, orderPrintPreparing, orderPrintReady } from '../services/topicsService';
import { monthNames } from '../services/dateService';

import Modal from './modal';

class PrintOrder extends Component {

  constructor(props) {
    super(props);

    this.state = {

      // Will contain the order info
      order: null,
    }
  }


  componentDidMount() {
    subscribe(orderPrintPreparing, () => this.preparingModal.show());
    subscribe(orderPrintReady, () => this.preparingModal.hide());
    subscribe(orderPrintDataSet, (action, order) => this.setState({ order }));
  }


  formatDate(date) {
    const dateTime = new Date(date);
    const monthName = monthNames[dateTime.getMonth()];
    const day = dateTime.getDay();
    const year = dateTime.getFullYear();
    return `${monthName} ${day}, ${year}`;
  }


  get images() {

    if ( !this.state.order.style || !this.state.order.style.images ) {
      return null
    }

    return (
      <div className="print-order__images">
        { this.state.order.style.images.map( (image, index) => (
          <img src={image.src} alt="" key={index}/>
        ))}
      </div>
    )
  }


  get steps() {

    if ( !this.state.order.style || !this.state.order.style.steps ) {
      return null
    }

    return (
      <div className="print-order__steps">
        <h3>Steps</h3>
        <ol>
          { this.state.order.style.steps.map( (step, index) => (
            <li key={index}>{step.text}</li>
          ))}
        </ol>
      </div>
    )
  }


  get preparingModalComponent() {
    return (
      <Modal ref={ ref => this.preparingModal = ref } persistent='true' >
        <div className="modal__heading">Preparing Images...</div>
      </Modal>
    )
  }



  render() {

    if ( !this.state.order ) {
      return null;
    }

    const {
      order_number,
      color,
      style,
      order_date,
      completion_date,
    } = this.state.order;

    return (
      <div className="print-page print-order">

        {/* HEADER */}
        <header className="print-order__header">
          <div className="print-order__number">
            <span className="print-order__number__prefix">Order #</span>
            <span className="print-order__number__digits">{order_number}</span>
          </div>
          <div className="print-order__details">
            <div className="print-order__details__left">
              <div className="print-order__details__left__item">
                {
                  color ? (
                    <Fragment>
                      <h3>Color</h3>
                      <p>
                        <span className="print-order__color-blot" style={{ backgroundColor: color.color_value }}></span>
                        {color.color_name}
                      </p>
                    </Fragment>
                  ) : null
                }
                {
                  style ? <h3>Style:</h3> : null
                }
              </div>
            </div>
            <div className="print-order__details__right">
              <div className="print-order__date print-order__date--order">
                <h3>Order Date</h3>
                <p>{this.formatDate(order_date)}</p>
              </div>
              {
                completion_date ?
                (
                  <div className="print-order__date print-order__date--completion">
                    <h3>Comp Date</h3>
                    <p>{this.formatDate(completion_date)}</p>
                  </div>
                ) : null
              }
            </div>
          </div>
        </header>
        {/* END HEADER */}

        {this.images}
        {this.steps}

        {this.preparingModalComponent}

      </div>
    )

  }
}

export default PrintOrder