import React from 'react';

function SVG({ name, className = '' }) {
  return (
    <svg className={`icon--${name} ${className}`}>
      <use xlinkHref={`/images/symbol/svg/sprite.symbol.svg#${name}`} />
    </svg>
  )
}

export default SVG;