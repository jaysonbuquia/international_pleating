import React, {Component} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import ColorService from '../services/colorService';

import Modal from '../components/modal';
import SVG from '../components/svg';
import Button from '../components/button';

class ColorActions extends Component {

  constructor(props) {
    super(props);
    this.state = {

      // Flag if this component should show up
      isOpen: false,

    };

    document.querySelector('body').addEventListener('click', e => {
      if (this.mounted) {
        this.setState({isOpen: false});
      }
    });
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  onClick(e) {
    e.stopPropagation();
    this.setState({isOpen: !this.state.isOpen});
  }

  confirmDelete(e) {
    e.preventDefault();
    this.confirmDeleteModal.show();
  }

  async proceedDelete() {
    this.confirmDeleteModal.hide();
    this.deletingModal.show();

    let response = await ColorService.delete(this.props.item.id);

    this.deletingModal.hide();
    this.showDeletedModal();
  }

  showDeletedModal() {
    this.deletedModal.show();
    setTimeout(() => {
      this.deletedModal.hide();
    }, 2000);
    setTimeout(() => {
      this.props.parentColorsList.fetchListData();
    }, 2300);
  }

  get sendToShipmentsCount() {

    if ( this.sendToShipmentsCountField ) {
      return this.sendToShipmentsCountField.value;
    }

    return 0;
  }

  get actions() {
    const { item } = this.props;
    const { id, order_number, status } = item;

    return [
      <Link to={`/colors/${id}`} className="colors-table__action" key="edit"><span>Edit color</span></Link>,
      <a href="#" onClick={this.confirmDelete.bind(this)} className="colors-table__action" key="delete"><span>Delete this color</span></a>
    ]
  }

  render() {
    const { item } = this.props;
    const { id, color_name, color_number } = item;

    return (
      <button className='colors-table__actions' data-order-id={id} onClick={this.onClick.bind(this)}>
        <div className={`colors-table__actions__popup ${ this.state.isOpen ? 'visible' : '' }`}>
          {this.actions}
        </div>

        {/* CONFIRM DELETE MODAL */}
        <Modal ref={ ref => this.confirmDeleteModal = ref } >
          <h3 className="modal__heading">{color_name}</h3>
          <div className="modal__text">Are you sure you want to delete this color?</div>
          <div className="modal__actions">
            <Button className="btn modal__dismiss" text="No, don’t delete" onClick={ e => this.confirmDeleteModal.hide() }></Button>
            <Button className="btn modal__proceed" text="Yes, delete" onClick={this.proceedDelete.bind(this)}></Button>
          </div>
        </Modal>

        {/* DELETING STATE MODAL */}
        <Modal ref={ ref => this.deletingModal = ref } persistent={true}>
          <h3 className="modal__heading">Deleting Color {color_name}</h3>
          <div className="modal__text">Please wait...</div>
        </Modal>

        {/* DELETED STATE MODAL */}
        <Modal ref={ ref => this.deletedModal = ref } persistent={true}>
          <h3 className="modal__heading">{color_name}</h3>
          <div className="modal__text">Has been deleted.</div>
          <img src="/images/trash-can.png" alt="" className="modal__graphic"/>
        </Modal>

      </button>
    )
  }
}

export default withRouter(ColorActions)
