import React, {Component} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { publish } from 'pubsub-js';

import StyleService from '../services/styleService';

import Modal from '../components/modal';
import SVG from '../components/svg';
import Button from '../components/button';

class StyleCardActions extends Component {

  constructor(props) {
    super(props);
    this.state = {

      // Flag if this component should show up
      isOpen: false,

    };

    document.querySelector('body').addEventListener('click', e => {
      if (this.mounted) {
        this.setState({isOpen: false});
      }
    });
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  onClick(e) {
    e.stopPropagation();
    this.setState({isOpen: !this.state.isOpen});
  }

  confirmDelete(e) {
    e.preventDefault();
    this.confirmDeleteModal.show();
  }

  async proceedDelete() {
    this.confirmDeleteModal.hide();
    this.deletingModal.show();

    let response = await StyleService.delete(this.props.item.id);

    this.deletingModal.hide();
    this.showDeletedModal();
  }

  showDeletedModal() {
    this.deletedModal.show();
    setTimeout(() => {
      this.deletedModal.hide();
    }, 2000);
    setTimeout(() => {
      this.props.parentStylesList.fetchListData();
    }, 2300);
  }

  get actions() {
    const { item } = this.props;
    const { id, order_number, status } = item;

    return [
      <Link to={`/styles/${id}`} className="style-card__action" key="edit"><span>Edit style</span></Link>,
      <a href="#" onClick={this.confirmDelete.bind(this)} className="style-card__action" key="delete"><span>Delete this style</span></a>
    ]
  }

  render() {
    const { item } = this.props;
    const { name, id, number } = item;

    return (
      <button className='style-card__actions' data-order-id={id} onClick={this.onClick.bind(this)}>

        •••

        <div className={`style-card__actions__popup ${ this.state.isOpen ? 'visible' : '' }`}>
          {this.actions}
        </div>

        {/* CONFIRM DELETE MODAL */}
        <Modal ref={ ref => this.confirmDeleteModal = ref } >
          <h3 className="modal__heading">Style #{name}</h3>
          <div className="modal__text">Are you sure you want to delete this style?</div>
          <div className="modal__actions">
            <Button className="btn modal__dismiss" text="No, don’t delete" onClick={ e => this.confirmDeleteModal.hide() }></Button>
            <Button className="btn modal__proceed" text="Yes, delete" onClick={this.proceedDelete.bind(this)}></Button>
          </div>
        </Modal>

        {/* DELETING STATE MODAL */}
        <Modal ref={ ref => this.deletingModal = ref } persistent={true}>
          <h3 className="modal__heading">Deleting Style #{name}</h3>
          <div className="modal__text">Please wait...</div>
        </Modal>

        {/* DELETED STATE MODAL */}
        <Modal ref={ ref => this.deletedModal = ref } persistent={true}>
          <h3 className="modal__heading">Style #{name}</h3>
          <div className="modal__text">Has been deleted.</div>
          <img src="/images/trash-can.png" alt="" className="modal__graphic"/>
        </Modal>

      </button>
    )
  }
}

export default withRouter(StyleCardActions)
