import React, {Component, Fragment} from 'react';

import SVG from '../svg';
import InputBlock from './input-block';

class Date extends Component {

  constructor(props) {
    super(props);
  }

  renderLabel ({label, id, icon}) {
    if ( label !== false ) {
      return (
        <Fragment>
          {icon ? (<SVG name={icon} />) : ''}
          <label htmlFor={id}>
            {label}
          </label>
        </Fragment>
      )
    } else {
      return '';
    }
  }

  render() {

    let { name, label, icon, defaultValue = '', id, rules = null, className = null, ...props } = this.props;

    defaultValue = defaultValue ? defaultValue.split(' ')[0] : '';

    return (
      <InputBlock animatedLabel={this.props.animatedLabel} className={className} rules={rules} {...props} ref={ ref => this.InputBlock = ref }>
        <input type="date" name={name} defaultValue={defaultValue} id={id} />
        {this.renderLabel({label, id, icon})}
      </InputBlock>
    )
  }

}

export default Date;