import React, {Component, Fragment} from 'react';

import SVG from '../svg';
import InputBlock from './input-block';

class Text extends Component {

  constructor(props) {
    super(props);
  }

  renderLabel ({label = null, id, icon}) {
    if ( label ) {
      return (
        <Fragment>
          {icon ? (<SVG name={icon} />) : ''}
          <label htmlFor={id}>
            {label}
          </label>
        </Fragment>
      )
    } else {
      return null;
    }
  }

  render() {

    let { animatedLabel = null, name, label = null, icon, id, rules = null, className = null, children = null, ...props } = this.props;

    return (
      <InputBlock animatedLabel={animatedLabel} className={className} rules={rules} {...props} ref={ ref => this.InputBlock = ref }>
        <input type="text" name={name} id={id} {...props} data-rules={rules}/>
        {this.renderLabel({label, id, icon})}
        {children}
      </InputBlock>
    )
  }

}

export default Text;