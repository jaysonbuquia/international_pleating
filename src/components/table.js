import React, {Component} from 'react';

class Table extends Component {

  constructor(props) {
    super(props);
  }

  /**
   * Generates thead contents from headings prop
   * @return {Array}
   */
  get headingItems() {
    return this.props.headings.map( (heading, key) => (
      <td key={key}>{heading}</td>
    ))
  }

  /**
   * Generates row elements from the rows prop
   * @return {Array}
   */
  get rows() {
    return this.props.rows.map( (row, key) => (
      <tr key={key}>{this.generateRowCells(row)}</tr>
    ))
  }

  /**
   * Generates td elements for a row
   * @param  {Array} cells - Array of Elements (React components)
   * @return {Array}
   */
  generateRowCells(cells) {
    return cells.map( (cell, key) => (
      <td key={key}>
        <div className="table__cell-content">{cell}</div>
      </td>
    ))
  }

  render() {
    let { className = '', ...props } = this.props;

    return (
      <table className={`table ${className}`} {...props}>
        <thead>
          <tr>
            {this.headingItems}
          </tr>
        </thead>
        <tbody>
          {this.rows}
        </tbody>
      </table>
    )
  }
}

export default Table