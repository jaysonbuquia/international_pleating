import { Component } from "react";

/**
 * Decorates React Component
 */

Component.prototype.setStateAsync = function (stateObject) {
  return new Promise(( resolve, reject ) => {
    try {
      this.setState(stateObject, resolve);
    } catch(err) {
      reject(err);
    }
  });
}