import React from 'react';

export default [
  {
    "id": 2,
    "name": "Style 2",
    "price": "100.45",
    "attribute": "SSS",
    "steps": "['Test step 1', 'Test step 2']",
    "created_at": "2018-01-27 13:12:24",
    "updated_at": "2018-01-27 13:12:24",
    "images": [
      {
        "id": 7,
        "created_at": "2018-01-28 13:30:13",
        "updated_at": "2018-01-28 13:30:13",
        "src": "https://placeimg.com/640/480/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
      },
      {
        "id": 8,
        "created_at": "2018-01-28 13:30:20",
        "updated_at": "2018-01-28 13:30:20",
        "src": "https://placeimg.com/480/320/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
      },
      {
        "id": 9,
        "created_at": "2018-01-28 13:30:27",
        "updated_at": "2018-01-28 13:30:27",
        "src": "https://placeimg.com/300/400/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
      }
    ],
    "number": "23424342",
    "category": "test_category"
  },
  {
    "id": 3,
    "name": "Style 2",
    "price": "0.46",
    "attribute": "SSS",
    "steps": "['Test step 1', 'Test step 2']",
    "created_at": "2018-01-27 13:21:46",
    "updated_at": "2018-01-27 13:21:46",
    "images": [
      {
        "id": 7,
        "created_at": "2018-01-28 13:30:13",
        "updated_at": "2018-01-28 13:30:13",
        "src": "https://placeimg.com/600/800/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
      }
    ],
    "number": "646456546",
    "category": "test_category"
  },
  {
    "id": 4,
    "name": "Style 3",
    "price": "0.46",
    "attribute": "SSS",
    "steps": "['Test step 1', 'Test step 2']",
    "created_at": "2018-01-27 13:21:51",
    "updated_at": "2018-01-27 13:21:51",
    "images": [
      {
        "id": 8,
        "created_at": "2018-01-28 13:30:20",
        "updated_at": "2018-01-28 13:30:20",
        "src": "https://placeimg.com/1280/1280/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
      },
      {
        "id": 9,
        "created_at": "2018-01-28 13:30:27",
        "updated_at": "2018-01-28 13:30:27",
        "src": "https://placeimg.com/1280/960/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
      }
    ],
    "number": "32422432",
    "category": "test_category"
  }
]