import React from 'react';

export default [
  {
    "id": 1,
    "created_at": "2018-01-28 14:10:59",
    "updated_at": "2018-01-28 14:10:59",
    "order_number": "12334534",
    "quantity": "1",
    "status": "new",
    "style_id": "3",
    "customer_id": "2",
    "color_id": "6",
    "order_date": "2017-10-15 00:00:00",
    "cancel_date": "2017-10-15 00:00:00",
    "completion_date": "2017-10-15 00:00:00",
    "customer_po_number": "24234",
    "ship_code": "werwerwer",
    "terms": "ewrwerwrew",
    "style": {
      "id": 3,
      "name": "Style 2",
      "price": "0.46",
      "attribute": "SSS",
      "steps": "['Test step 1', 'Test step 2']",
      "created_at": "2018-01-27 13:21:46",
      "updated_at": "2018-01-27 13:21:46",
      "images": [
        {
          "id": 7,
          "created_at": "2018-01-28 13:30:13",
          "updated_at": "2018-01-28 13:30:13",
          "src": "http://ip.local/images/1517146213_IMG_20180122_164415.jpg",
          "title": null,
          "alt": null,
          "width": "3264",
          "height": "2448"
        }
      ],
      "number": "646456546",
      "category": "test_category"
    },
    "color": {
      "id": 6,
      "created_at": "2018-01-26 11:25:11",
      "updated_at": "2018-01-26 11:25:11",
      "color_number": "234535353",
      "color_name": "Blue",
      "color_value": "00f"
    },
    "customer": {
      "id": 2,
      "account_number": "234",
      "name": "Test Customer 2",
      "email": "test1235@email.com",
      "city": "Other City",
      "state": "CA",
      "notes": "fsd sfasfsd fsf sf ssdf s",
      "created_at": "2018-01-28 05:51:23",
      "updated_at": "2018-01-28 05:51:23",
      "terms_code": "2423sfs",
      "ship_code": "sfs242",
      "shipping_number": "132424",
      "credit_limit": "123.23",
      "shipping_address_same": "0",
      "shipping_address": "sfsfsd",
      "shipping_city": "City",
      "shipping_state": "CA",
      "shipping_country": "US"
    }
  },
  {
    "id": 2,
    "created_at": "2018-01-28 14:28:58",
    "updated_at": "2018-02-03 05:47:39",
    "order_number": "454353345",
    "quantity": "12",
    "status": "printing",
    "style_id": "3",
    "customer_id": "1",
    "color_id": "9",
    "order_date": "2017-10-15 00:00:00",
    "cancel_date": "2017-10-15 00:00:00",
    "completion_date": "2017-10-15 00:00:00",
    "customer_po_number": "24234",
    "ship_code": "werwerwer",
    "terms": "ewrwerwrew",
    "style": {
      "id": 3,
      "name": "Style 2",
      "price": "0.46",
      "attribute": "SSS",
      "steps": "['Test step 1', 'Test step 2']",
      "created_at": "2018-01-27 13:21:46",
      "updated_at": "2018-01-27 13:21:46",
      "images": [
        {
          "id": 7,
          "created_at": "2018-01-28 13:30:13",
          "updated_at": "2018-01-28 13:30:13",
          "src": "http://ip.local/images/1517146213_IMG_20180122_164415.jpg",
          "title": null,
          "alt": null,
          "width": "3264",
          "height": "2448"
        }
      ],
      "number": "646456546",
      "category": "test_category"
    },
    "color": {
      "id": 9,
      "created_at": "2018-01-26 11:28:24",
      "updated_at": "2018-01-26 11:28:24",
      "color_number": "9875644",
      "color_name": "Green",
      "color_value": "0f0"
    },
    "customer": {
      "id": 1,
      "account_number": "123",
      "name": "Test Customer 1",
      "email": "test1234@email.com",
      "city": "Other City",
      "state": "CA",
      "notes": "fsd sfasfsd fsf sf ssdf s",
      "created_at": "2018-01-28 05:45:23",
      "updated_at": "2018-01-28 05:45:23",
      "terms_code": "2423sfs",
      "ship_code": "sfs242",
      "shipping_number": "132424",
      "credit_limit": "123.23",
      "shipping_address_same": "0",
      "shipping_address": "sfsfsd",
      "shipping_city": "City",
      "shipping_state": "CA",
      "shipping_country": "US"
    }
  }
]