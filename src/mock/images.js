import React from 'react';

export default [
    {
        "id": 7,
        "created_at": "2018-01-28 13:30:13",
        "updated_at": "2018-01-28 13:30:13",
        "src": "https://placeimg.com/640/480/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
    },
    {
        "id": 8,
        "created_at": "2018-01-28 13:30:20",
        "updated_at": "2018-01-28 13:30:20",
        "src": "https://placeimg.com/480/320/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
    },
    {
        "id": 9,
        "created_at": "2018-01-28 13:30:27",
        "updated_at": "2018-01-28 13:30:27",
        "src": "https://placeimg.com/300/400/any",
        "title": null,
        "alt": null,
        "width": "3264",
        "height": "2448"
    }
]