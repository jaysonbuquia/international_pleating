import AuthService from './authService';
/**
 * API Service
 *
 * Centralized API requests for Front End
 *
 * USAGE:
 *   apiService.request( endpointName, options )
 *
 * OPTIONS (optional depending on endpoint requirements)
 *   params {Object} - URL params. e.g., { params: {id: '123-company-id'} } (Please see ENDPOINTS constant below)
 *   data {Object} - Request body, usually for POST/PATCH requests.
 *
 */


const BASE_PATH = '/api';

function trimURLSlashes( text ) {
  return text.replace(/^[\s\/]*|[\s\/]*$/g, '');
}


export default {

  async request(path = '/api/me', method = 'GET', {params = {}, data = {} } = {}) {
    let url = this.getEndpointURL(path, params);
    let headers = new Headers({
      'Authorization': AuthService.token,
    });
    let fetchParams = { headers, method };

    // Set additional headers and data acc. to method/endpoint
    // if ( /login/.test(path) ) {
    //   fetchParams.body = JSON.stringify(data);
    // }
    if ( /(post|patch)/i.test(method) ) {
      fetchParams.headers.append('Content-Type', 'application/x-www-form-urlencoded');
      fetchParams.body = Object.entries(data).map( entry => entry.join('=').trim() ).join('&').trim();
    }

    // Add auth if necessary
    // if ( ! /login|forgotPassword/.test(endpoint) ) {
    //   fetchParams.headers.append('X-Authorization', `Bearer ${token}`);
    // }

    // Send request
    let response = await fetch( url, fetchParams);

    if ( !response.ok ) {
      let error = await response;
      throw error;
    } else {
      return response.json();
    }
  },


  getEndpointURL( path, params = {} ) {
    let endpointURL = `/${trimURLSlashes(BASE_PATH)}/${trimURLSlashes(path)}`;
    let paramsList = Object.entries(params);

    // Replace url params if any
    if ( paramsList.length ) {
      paramsList.forEach( param => endpointURL = endpointURL.replace(new RegExp(`\{${param[0]}\}`, 'gi'), param[1]) );
    }

    return endpointURL;
  }

}