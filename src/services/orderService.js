import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class OrderService extends EntityService {
  constructor(args) {
    super(args);
  }

  get emptyOrder() {
    return {
      "order_number": "",
      "quantity": '',
      "status": "new",
      "style_id": "",
      "customer_id": "",
      "color_id": "3",
      "order_date": "",
      "cancel_date": "",
      "completion_date": "",
      "customer_po_number": "",
      "ship_code": "",
      "terms": "",
      "quantity_shipment": "",
      "quantity_production": "",
      "style": {
        "id": 7,
        "name": "",
        "price": "",
        "attribute": "",
        "steps": "[]",
        "images": [],
        "number": "",
        "category": ""
      },
      "color": {
        "id": 3,
        "color_number": "",
        "color_name": "",
        "color_value": ""
      },
      "customer": {
        "account_number": "",
        "name": "",
        "email": "",
        "city": "",
        "state": "",
        "notes": "",
        "terms_code": "",
        "ship_code": "",
        "shipping_number": "",
        "credit_limit": "",
        "shipping_address_same": 1,
        "shipping_address": "",
        "shipping_city": "",
        "shipping_state": "",
        "shipping_country": "",
        "phone": "",
        "address": "",
        "country": "",
      }
    }
  }

  async withStatus({status = 'new', page = 1, count = 20, search = null }) {
    let [path, method] = this.endpoints.status;
    let entries = await apiService.request(path, method, {params: {page, status, count, search}});
    return entries;
  }

  /**
   * Available status: new, production, shipment, hold, done, cancelled
   */
  async sendToProductions( order ) {
    order.status = 'production';
    order.quantity_production = Number(order.quantity);
    return await this.use(order).update();
  }

  async markAsDone( order ) {
    order.status = 'done';

    // Ensure empty items in production and shipment
    order.quantity_production = 0;
    order.quantity_shipment = 0;

    return await this.use(order).update();
  }

  async holdProduction( order ) {
    order.status = 'hold';
    return await this.use(order).update();
  }

  async delete( order ) {
    // Instead of deleting an order data, it should only be cancelled
    order.status = 'cancelled';

    // Ensure empty items in production and shipment
    order.quantity_production = 0;
    order.quantity_shipment = 0;

    return await this.use(order).update();
  }
}

decorateEntity(OrderService, {
  acceptedAttributes: [
    'id',
    'order_number',
    'style_id',
    'customer_id',
    'color_id',
    'quantity',
    'quantity_shipment', // Should be an array of integers (quantity per shipment)
    'quantity_production',
    'status',
    'order_date',
    'cancel_date',
    'completion_date',
    'customer_po_number',
    'ship_code',
    'terms',
  ],
  endpoints: {
    get: ['/orders/{id}', 'GET'],
    all: ['/orders', 'GET'],
    page: ['/orders/{page}', 'GET'],
    status: ['/orders?status={status}&page={page}&count={count}&search={search}', 'GET'],
    save: ['/orders', 'POST'],
    update: ['/orders/{id}', 'PATCH'],
    delete: ['/orders/{id}', 'DELETE'],
  }
});

export default (new OrderService);