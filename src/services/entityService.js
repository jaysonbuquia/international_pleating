import apiService from './apiService';

class EntityService  {
  constructor() {
    this.entries = [];
  }

  use(data) {
    let filteredData = {};
    if ( this.acceptedAttributes ) {
      for ( let attribute of this.acceptedAttributes ) {
        if ( attribute in data && data[attribute] !== null && data[attribute] !== undefined ) {
          filteredData[attribute] = data[attribute];
        }
      }
    }
    this.data = {...filteredData};
    return this
  }

  async all() {
    let [path, method] = this.endpoints.all;
    return await apiService.request(path, method);
  }

  async get( id ) {
    let [path, method] = this.endpoints.get;
    let params = { id };
    return await apiService.request( path, method, {params} );
  }

  page( page = 1, limit = 20 ) {

  }

  // Sends a post request for new entry
  async save() {
    let [path, method] = this.endpoints.save;
    let data = this.data;
    return await apiService.request( path, method, {data} )
  }

  // Sends a patch request for an existing entry
  async update() {
    let [path, method] = this.endpoints.update;
    let data = this.data;
    let { id } = data;
    return await apiService.request( path, method, { params: { id }, data} )
  }

  // Sends a delete request for an existing entry
  async delete( id ) {
    let [path, method] = this.endpoints.delete;
    let data = this.data;
    return await apiService.request( path, method, { params: { id }} )
  }
}


function decorateEntity( entityClass, data = {} ) {
  for ( let key in data ) {
    entityClass.prototype[key] = data[key];
  }
}


export default EntityService
export { decorateEntity }