import PubSub from 'pubsub-js';

import { headerUpdateCounts } from '../services/topicsService';
import apiService from './apiService';


class DashboardService {
	constructor(args) {
		// PubSub.subscribe(headerUpdateCounts, (msg, data) => console.log(data));
	}

	async updateHeaderCounts() {
		let result = await apiService.request('/dashboard', 'GET');
		PubSub.publish( headerUpdateCounts, result );
	}

}

export default (new DashboardService);