import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class CustomerService extends EntityService {
  constructor(args) {
    super(args);
  }

  async search ( name = '' ) {
    let [path, method] = this.endpoints.search;
    let entries = await apiService.request(path, method, {data: { name }});
    return entries;
  }

  get emptyCustomer () {
    return {
      account_number: '',
      name: '',
      email: '',
      address: '',
      city: '',
      state: '',
      country: '',
      notes: '',
      customer: '',
      phone: '',
      terms_code: '',
      ship_code: '',
      shipping_number: '',
      credit_limit: '',
      shipping_address_same: false,
      shipping_address: '',
      shipping_city: '',
      shipping_state: '',
      shipping_country: '',
    }
  }
}

decorateEntity(CustomerService, {
  acceptedAttributes: [
    'id', 'account_number', 'name', 'email', 'address', 'city', 'state', 'country', 'notes', 'phone', 'contact',
    'terms_code', 'ship_code', 'shipping_number', 'credit_limit',
    'shipping_address_same', 'shipping_address', 'shipping_city', 'shipping_state', 'shipping_country'
  ],
  endpoints: {
    search: ['/customers/search', 'POST'],
    get: ['/customers/{id}', 'GET'],
    all: ['/customers', 'GET'],
    page: ['/customers/{page}', 'GET'],
    status: ['/customers/{status}/{page}?count={count}', 'GET'],
    save: ['/customers/', 'POST'],
    update: ['/customers/{id}', 'PATCH'],
    delete: ['/customers/{id}', 'DELETE'],
  }
});

export default (new CustomerService);