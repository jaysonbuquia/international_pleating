import { publish } from 'pubsub-js';
import { orderPrintDataSet, orderPrintPreparing, orderPrintReady } from './topicsService';
import { invoicePrintDataSet, invoicePrintPreparing, invoicePrintReady } from './topicsService';

class PrintService {
	constructor(args) {
		// code
	}

	use(data = {}, type = 'order') {
		if ( type === 'order' ) {
			publish(orderPrintDataSet, data);
		} else {
			publish(invoicePrintDataSet, data);
		}
	}

	async printOrder(data) {
		this.use(data, 'order');

		// Prepare images
		publish(orderPrintPreparing);
		await this.preloadImages(data.style.images);

		publish(orderPrintReady);
		this.print();
	}

	async printInvoice(orders) {
		this.use(orders, 'invoice');

		// Just emit a "preparing" event
		publish(invoicePrintPreparing);

		publish(invoicePrintReady);
		this.print();
	}

	preloadImages(images) {
		return new Promise((resolve, reject) => {
			if ( !images || !images.length ) {
				return resolve();
			}

			let preloaded = 0;

			images.forEach( image => {

				let _image = new Image;

				_image.onload = function () {
					preloaded++;

					if ( preloaded === images.length ) {
						resolve();
					}
				};

				_image.onerror = function () {
					preloaded++;

					if ( preloaded === images.length ) {
						resolve();
					}
				}

				_image.src = image.src;

			});
		});

	}

	print() {
		setTimeout(() => window.print(), 500);
	}
}


export default (new PrintService);