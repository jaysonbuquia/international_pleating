import { publish } from 'pubsub-js';

import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';
import userDataResponse from './topicsService';

// Symbols to be used as protected property accessors
const userData = Symbol();

class AuthService extends EntityService {
  constructor(args) {
    super(args);

    this[userData] = null;
  }

  async validateToken() {
    return await apiService.request('/auth/validate_token', 'POST', { data: {token: this.token} });
  }

  async login({ email, password }) {
    const response = await apiService.request('/auth/login', 'POST', { data: { email, password} });

    if ( response.success ) {
      this.saveToken(response.token);
      window.location.href = '/';
      return;
    }

    const error = await response.json();
    throw error.message;
  }

  logout() {
    this.removeToken();
  }

  saveToken( token ) {
    window.localStorage.setItem('token', token);
  }

  removeToken() {
    window.localStorage.removeItem('token');
  }

  async getUserData() {

    if ( this[userData] ) {
      return this[userData];
    }

    try {
      const response = await apiService.request('/users/me', 'GET');

      this[userData] = response;

      publish(userDataResponse, this[userData]);

      return this[userData];

    } catch (err) {
      console.error(err);
      throw err;
    }


  }

  get isLoggedIn() {
    return this.token ? true : false;
  }

  get token() {
    return  window.localStorage.getItem('token');
  }
}

export default (new AuthService);