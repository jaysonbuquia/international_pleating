import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class StyleService extends EntityService {
  constructor(args) {
    super(args);
  }

  // async search( name = '' ) {
  //   let [path, method] = this.endpoints.search;
  //   let entries = await apiService.request(path, method, {data: { name }});
  //   return entries;
  // }

  get emptyStyle () {
    return {
      'name': '',
      'price': '',
      'number': '',
      'category': '',
      'attribute': '',
      'steps': [],
      'images': [],
    }
  }
}

decorateEntity(StyleService, {
  acceptedAttributes: [
    'id', 'name', 'price', 'number', 'category', 'attribute', 'steps', 'images', 'weight'
  ],
  endpoints: {
    search: ['/styles/search', 'POST'],
    get: ['/styles/{id}', 'GET'],
    all: ['/styles', 'GET'],
    page: ['/styles/{page}', 'GET'],
    status: ['/styles/{status}/{page}?count={count}', 'GET'],
    save: ['/styles/', 'POST'],
    update: ['/styles/{id}', 'PATCH'],
    delete: ['/styles/{id}', 'DELETE'],
  }
});

export default (new StyleService);