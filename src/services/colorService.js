import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class ColorService extends EntityService {
  constructor(args) {
    super(args);
  }

  get emptyColor() {
    return {
      'color_name': '',
      'color_number': '',
      'color_value': '#d8d8d8',
    }
  }
}

decorateEntity(ColorService, {
  acceptedAttributes: [
    'id', 'color_name', 'color_number', 'color_value'
  ],
  endpoints: {
    get: ['/colors/{id}', 'GET'],
    all: ['/colors', 'GET'],
    page: ['/colors/{page}', 'GET'],
    status: ['/colors/{status}/{page}?count={count}', 'GET'],
    save: ['/colors', 'POST'],
    update: ['/colors/{id}', 'PATCH'],
    delete: ['/colors/{id}', 'DELETE'],
  }
});

export default (new ColorService);