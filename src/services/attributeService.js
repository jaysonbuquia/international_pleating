import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class AttributeService extends EntityService {
  constructor(args) {
    super(args);
  }

  get emptyColor() {
    return {
      'name': '',
    }
  }
}

decorateEntity(AttributeService, {
  acceptedAttributes: [
    'id', 'name',
  ],
  endpoints: {
    get: ['/attributes/{id}', 'GET'],
    all: ['/attributes', 'GET'],
    page: ['/attributes/{page}', 'GET'],
    status: ['/attributes/{status}/{page}?count={count}', 'GET'],
    save: ['/attributes', 'POST'],
    update: ['/attributes/{id}', 'PATCH'],
    delete: ['/attributes/{id}', 'DELETE'],
  }
});

export default (new AttributeService);