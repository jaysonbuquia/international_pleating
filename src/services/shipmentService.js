import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class ShipmentService extends EntityService {
  constructor(args) {
    super(args);
  }
}

decorateEntity(ShipmentService, {
  acceptedAttributes: [
    'id',
    'order_id',
    'quantity',
    'status',
  ],
  endpoints: {
    get: ['/shipments/{order_id}', 'GET'],
    all: ['/shipments', 'GET'],
    page: ['/shipments/{page}', 'GET'],
    save: ['/shipments', 'POST'],
    update: ['/shipments/{id}', 'PATCH'],
    delete: ['/shipments/{id}', 'DELETE'],
  }
});

export default (new ShipmentService);