import { decorateEntity } from './entityService';
import EntityService from './entityService';
import apiService from './apiService';

class ImageService extends EntityService {
  constructor(args) {
    super(args);
  }

  // We will use the Promise approach to have more control on async
  upload( files ) {

    let totalFiles = files.length;
    let uploaded = [];
    let [ path, method ] = this.endpoints.upload;

    return new Promise(function (resolve, reject) {
       for ( let index = 0; index < files.length; index++ ) {
        let formData = new FormData;

        formData.append('image', files[index]);

        fetch( `/api${path}`, {
          method: method,
          body: formData,
        })
        .then( response => response.json() )
        .then( data => {
          uploaded.push(data);
          if (uploaded.length === totalFiles) {
            resolve(uploaded);
          }
        })
        .catch(reject);
      }
    });
  }
}

decorateEntity(ImageService, {
  acceptedAttributes: [
    'id', 'src', 'title', 'alt', 'width', 'height',
  ],
  endpoints: {
    get: ['/images/{id}', 'GET'],
    all: ['/images', 'GET'],
    page: ['/images/{page}', 'GET'],
    status: ['/images/{status}/{page}?count={count}', 'GET'],
    save: ['/images', 'POST'],
    update: ['/images/{id}', 'PATCH'],
    delete: ['/images/{id}', 'DELETE'],
    upload: ['/images', 'POST'],
  }
});

export default (new ImageService);