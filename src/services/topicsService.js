export const headerUpdateCounts = 'HEADER_UPDATE_COUNTS';

export const userDataResponse = 'USER_DATA_RESPONSE';

export const orderPrintDataSet = 'ORDER_PRINT_DATA_SET';

export const orderPrintPreparing = 'ORDER_PRINT_PREPARING';

export const orderPrintReady = 'ORDER_PRINT_READY';

export const invoicePrintDataSet = 'INVOICE_PRINT_DATA_SETSET';

export const invoicePrintPreparing = 'INVOICE_PRINT_PREPARING';

export const invoicePrintReady = 'INVOICE_PRINT_READY';
