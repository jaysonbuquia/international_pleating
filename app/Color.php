<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = [
      'color_name', 'color_number', 'color_value'
    ];

    /**
     * Validation rules on a post request
     */
    public static $post_rules = [
      'color_number' => 'required|numeric',
      'color_name' => 'required',
      'color_value' => [
          'required',
          'regex:/^#([0-9a-f]{3}|[0-9a-f]{6})$/', // HEX
      ],
    ];

    /**
     * Validation rules on a patch request
     */
    public static $patch_rules = [
      'color_number' => 'integer',
      'color_value' => [ 'regex:/^#([0-9a-f]{3}|[0-9a-f]{6})$/'], // HEX
    ];
}
