<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'photo_id',
        'role' // staff | admin | superadmin
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Default attributes for a user
     *
     */
    protected $attributes = [
        'role' => 'staff'
    ];

    /**
     * Default attributes for a user
     *
     */
    public static $rules = [
        'first_name' => 'string|required|',
        'email' => 'email|required|',
        'password' => 'string|required|min:8',
        'role' => 'string|required|in:staff,admin,superadmin'
    ];
}
