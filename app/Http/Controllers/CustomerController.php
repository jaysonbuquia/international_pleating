<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->input('email');
        $account_number = $request->input('account_number');

        $fields = $request->all();
        $fields['shipping_number'] = $fields['shipping_number'] ? $fields['shipping_number'] : '';
        $fields['shipping_address_same'] = array_key_exists('shipping_address_same', $fields) && $fields['shipping_address_same'] === "on" ? 1 : 0;

        // Validate values
        $validator = Validator::make($fields, Customer::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 400);
        }

        // Check for existing customer with same email
        $existing_customer = Customer::where('email', $email)->first();
        if ( $existing_customer ) {
            return response([
                'success' => false,
                'message' => 'Email is already taken.',
            ], 409);
        }

        // Check for existing customer with same account_number
        $same_account_number = Customer::where('account_number', $account_number)->first();
        if ( $same_account_number ) {
            return response([
                'success' => false,
                'message' => 'Account number is already taken.',
            ], 409);
        }

        // Otherwise, Create new customer
        $new_customer = Customer::create($fields);
        return [
            'success' => true,
            'data' => $new_customer
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existing_customer = Customer::where('id', $id)->first();

        if ( ! $existing_customer ) {
            return response([
                'success' => false,
                'message' => 'Customer does not exist',
            ], 404);
        }

        return Customer::where('id', $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existing_customer = Customer::where('id', $id)->first();

        $fields = $request->all();
        $fields['shipping_number'] = $fields['shipping_number'] ? $fields['shipping_number'] : '';
        $fields['shipping_address_same'] = array_key_exists('shipping_address_same', $fields) && $fields['shipping_address_same'] === "on" ? 1 : 0;

        foreach ($fields as $key => $value) {
            $existing_customer[$key] = $value;
        }

        if ( !$existing_customer ) {
            return response([
                'success' => false,
                'message' => 'Unable to update an non-existing customer.'
            ], 404);
        }

        // Validate values
        $validator = Validator::make($existing_customer->toArray(), Customer::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        // Check for existing customer with email
        $email = $existing_customer->email;
        $same_email = Customer::where('email', $email)->first();
        if ( $same_email && $existing_customer->id !== $same_email->id) {
            return response([
                'success' => false,
                'errors' => ['email' => ['Email is already taken.']],
            ], 409);
        }

        // Check for existing customer with account_number
        $account_number = $existing_customer->account_number;
        $same_account_number = Customer::where('account_number', $account_number)->first();
        if ( $same_account_number && $existing_customer->id !== $same_account_number->id) {
            return response([
                'success' => false,
                'message' => 'Account number is already taken.',
            ], 409);
        }

        if ( $existing_customer->save() ) {
            return [
                'success' => true,
                'data' => $existing_customer,
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Unable to update customer due to some error.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing_customer = Customer::where('id', $id)->first();

        if ( !$existing_customer ) {
            return response([
                'success' => false,
                'message' => 'Unable to delete an non-existing customer.'
            ], 404);
        }

        $is_deleted = Customer::destroy($id);

        if ( $is_deleted ) {
            return [
                'success' => true,
                'message' => 'Successfuly deleted customer.',
            ];
        }

        return response([
            'success' => false,
            'message' => 'Unable to delete customer due to an error.',
        ], 404);
    }

    /**
     * Search the customers by name
     * @param  Request $request
     * @return Response
     */
    public function searchByName(Request $request)
    {
        $name = $request->get('name');
        $matches = Customer::where('name','like',"%$name%")->get();
        return $matches;
    }
}
