<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Shipment;

class DashboardController extends Controller
{
    public function counts(Request $request)
    {
    	$all_orders = Order::all();

    	$new_orders = Order::where(['status'=> 'new'])->count();
    	// $history = Order::whereIn('status', ['done', 'cancelled'])->count();
    	$productions = Order::where('quantity_production', '>', 0)->count();

		$shipment_orders_query = Order::where('quantity_shipment', '>', 0)->get();
		$shipment_orders_ids = $shipment_orders_query->pluck('id');
		$shipments = Shipment::whereIn('order_id', $shipment_orders_ids)->count();

    	return compact('new_orders', 'history', 'productions', 'shipments');
    }
}
