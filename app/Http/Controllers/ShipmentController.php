<?php

namespace App\Http\Controllers;

use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use App\Order;
use App\Shipment;
use Validator;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order_controller = new OrderController;
        $count = $request->has('count') ? $request->get('count') : 20;
        $page = $request->has('page') ? $request->get('page') : 1;

        // Get orders with shipment
        $orders = Order::where('quantity_shipment','>',0)
                            ->get();

        // Attach complete order details (includes color, style, customer, etc.)
        $order_shipment = array_map(function ($item) use (&$order_controller)
        {
            $order = $order_controller->show($item['id']);
            // Apply "shipment" status even for production
            $order->status = 'shipment';
            return $order;
        }, $orders->toArray());

        // Orders as object for mapping
        $order_map = [];
        foreach ($order_shipment as $order) {
            $order_map[$order->id] = $order;
        }

        // Get referenced shipments
        $shipments_list = [];
        $order_ids = $orders->pluck('id');
        $shipments = Shipment::whereIn('order_id', $order_ids)->get()->toArray();

        // Ensure that shipments of the same order are adjacent in the list
        foreach ($order_ids as $order_id) {
            $adjacent_shipments = array_filter($shipments, function ( $shipment ) use($order_id)
            {
                return $shipment['order_id'] == $order_id ? true : false;
            });
            $shipments_list = array_merge($shipments_list, $adjacent_shipments);
        }

        // Attach order data into each shipment, force to use "shipment" status
        return array_map(function ($shipment) use(&$order_map)
        {
            $shipment['order'] = $order_map[$shipment['order_id']];
            return $shipment;
        }, $shipments_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_controller = new OrderController;

        // Validate values
        $validator = Validator::make($request->all(), Shipment::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 400);
        }

        // Otherwise, Create new shipment
        $new_shipment = Shipment::create($request->all());

        // Update Referenced order's quantities
        $order = Order::where('id', $request->get('order_id'))->first();

        if ( $order ) {
            $quantity = $request->get('quantity');
            $order->quantity_production -= $quantity;
            $order->quantity_shipment += $quantity;

            // Ensure production quantity is not less than 0
            $order->quantity_production = $order->quantity_production < 0 ? 0 : $order->quantity_production;

            // Update order status to shipment if there is no more items in production
            if ( $order->quantity_production === 0 ) {
                $order->status = 'shipment';
            }

            $order->save();
            // Get updated order data
            $new_shipment->order = $order_controller->show($order->id);;
        }

        return [
            'success' => true,
            'data' => $new_shipment,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
