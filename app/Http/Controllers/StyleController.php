<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Style;
use App\Image;
use Validator;

class StyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $styles = [];

        foreach (Style::all() as $style) {
            $images = Image::whereIn('id', json_decode($style->images))->get();
            $style->images = $images;
            $style->steps = json_decode($style->steps);
            array_push($styles, $style);
        }

        return $styles;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate values
        $validator = Validator::make($request->all(), Style::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        // Check for existing style
        // $existing_color = Style::where('color_value', $hex)->first();
        // if ( $existing_color ) {
        //     return response([
        //         'success' => false,
        //         'message' => 'Color value is already taken.',
        //     ], 409);
        // }

        // Otherwise, Create new style
        $new_style = Style::create($request->all());
        return [
            'success' => true,
            'data' => $new_style
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existing_style = Style::where('id', $id)->first();

        if ( ! $existing_style ) {
            return response([
                'success' => false,
                'message' => 'Style does not exist',
            ], 404);
        }

        // Get style images
        $images = Image::whereIn('id', json_decode($existing_style->images))->get();
        $existing_style->images = $images->toArray();
        $existing_style->steps = json_decode($existing_style->steps);

        return $existing_style;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existing_style = Style::where('id', $id)->first();

        foreach ($request->all() as $key => $value) {
            $existing_style[$key] = $value;
        }

        if ( !$existing_style ) {
            return response([
                'success' => false,
                'message' => 'Unable to update an non-existing color.'
            ], 404);
        }

        // Validate values
        $validator = Validator::make($existing_style->toArray(), Style::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        if ( $existing_style->save() ) {
            return [
                'success' => true,
                'message' => 'Style updated.',
                'data' => $existing_style,
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Unable to update style due to some error.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing_style = Style::where('id', $id)->first();

        if ( !$existing_style ) {
            return response([
                'success' => false,
                'message' => 'Unable to delete an non-existing style.'
            ], 404);
        }

        $is_deleted = Style::destroy($id);

        if ( $is_deleted ) {
            return [
                'success' => true,
                'message' => 'Successfuly deleted style.',
            ];
        }

        return response([
            'success' => false,
            'message' => 'Unable to delete an non-existing style due to an error.',
        ], 404);
    }
}
