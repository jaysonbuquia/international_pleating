<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Image;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->input('email');

        // Validate values
        $validator = Validator::make($request->all(), User::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        // Check for existing user
        $existing_user = User::where('email', $email)->first();
        if ( $existing_user ) {
            return response([
                'success' => false,
                'message' => 'Email is already taken.',
            ], 409);
        }

        // Otherwise, Create new user
        $user_data = $request->all();
        // Encrypt password
        $user_data['password'] = bcrypt($user_data['password']);
        // Create user
        $new_user = User::create($user_data);
        return [
            'success' => true,
            'data' => $new_user
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();

        if ( !$user ) {
            return null;
        }

        $photo = Image::where('id', $user->photo_id)->first();
        $user->photo = $photo;

        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existing_user = User::where('id', $id)->first();

        if ( !$existing_user ) {
            return response([
                'success' => false,
                'message' => 'Unable to update an non-existing user.'
            ], 404);
        }

        $email = $request->input('email');
        $similar_user = User::where('email', $email)->first();

        if ( $similar_user && $similar_user->id !== $id ) {
            return response([
                'success' => false,
                'message' => 'Email is already taken.'
            ], 409);
        }

        if ( $existing_user->update($request->all()) ) {
            return [
                'success' => true,
                'message' => 'User updated.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
