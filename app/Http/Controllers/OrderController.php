<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\StyleController;
use App\Color;
use App\Customer;
use App\Style;
use App\Order;
use App\Image;
use Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order_controller = new $this;
        $status = $request->query('status');
        $search = $request->query('search');

        if ($status) {
            $orders = Order::whereIn('status', explode(',', $status));
        } else {
            $orders = Order::query();
        }

        // if ( $search ) {
        //     $orders->where('name','like', '%'. $search . '%');
        // }

        $orders = $orders->get();

        $orders_data = [];
        foreach ($orders as $order) {
            $order_data = $order_controller->show($order->id);
            array_push($orders_data, $order_data);
        }

        return $orders_data;
    }


    // public function withStatus(Request $request)
    // {
    //     $order_controller = new $this;
    //     $count = $request->get('count');
    //     $count = $count ? $count : 20;
    //     $skip = $count * ($page - 1);

    //     $orders = Order::whereIn('status', explode(',', $status))
    //                 ->skip($skip)
    //                 ->take($count)
    //                 ->get();

    //     $orders_data = [];
    //     foreach ($orders as $order) {
    //         $order_data = $order_controller->show($order->id);
    //         array_push($orders_data, $order_data);
    //     }

    //     return $orders_data;
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fields = [];

        foreach ($request->all() as $key => $value) {
            $value = $value ? $value : null;
            $fields[$key] = $value;
        }

        // Validate values
        $validator = Validator::make($fields, Order::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 400);
        }

        // Otherwise, Create new style
        $new_order = Order::create($fields);
        return [
            'success' => true,
            'data' => $new_order
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $style_controller = new StyleController();
        $existing_order = Order::where('id', $id)->first();

        if ( ! $existing_order ) {
            return response([
                'success' => false,
                'message' => 'Order does not exist',
            ], 404);
        }

        // Get order style, color, and customer
        $style = Style::where('id', $existing_order->style_id)->first();
        $style->steps = json_decode($style->steps);
        $style->images = Image::whereIn('id', json_decode($style->images))->get();
        $color = Color::where('id', $existing_order->color_id)->first();
        $customer = Customer::where('id', $existing_order->customer_id)->first();
        $existing_order->style = $style;
        $existing_order->color = $color;
        $existing_order->customer = $customer;

        return $existing_order;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existing_order = Order::where('id', $id)->first();

        foreach ($request->all() as $key => $value) {

            if ( $key === 'cancel_date' || $key === 'completion_date' ) {
                // Ensure null formats for empty fields
                $value = $value ? $value : null;
            }

            $existing_order[$key] = $value;
        }

        if ( !$existing_order ) {
            return response([
                'success' => false,
                'message' => 'Unable to update an non-existing order.'
            ], 404);
        }

        // Validate values
        $validator = Validator::make($existing_order->toArray(), Order::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        if ( $existing_order->save() ) {
            return [
                'success' => true,
                'message' => 'Order updated.',
                'data'    => $existing_order,
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Unable to update order due to some error.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // There seems to be no instance of an order being removed,
        // So setting up this endpoint makes no sense
    }
}
