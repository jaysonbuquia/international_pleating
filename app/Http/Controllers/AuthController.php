<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Token;
use App\User;
use \DateTime;

class AuthController extends Controller
{
    public function login(Request $request)
    {
      $email = $request->input('email');
      $password = $request->input('password');

      $matched_user = User::where(compact('email', 'password'))->first();

      if ( $matched_user ) {

        // Get user role
        $user = User::where('email', $request->input('email'))->first();

        // Create new token
        $token = $this::generate_token($user);

        return response([
          'success' => true,
          'token' => $token->hash,
        ], 200);

      } else {
        return response([
          'success' => false,
          'message' => 'Invalid email/password',
        ], 401);
      }
    }


    // Validates a token if not yet expired
    public function validate_token(Request $request = null)
    {
      $token = $request->input('token');
      $is_valid = $this::is_token_valid($token);

      if ( $is_valid ) {
        $token = Token::where('hash', $token)->first();
        $user = User::findOne($token->user_id);

        return [
          'success' => true,
          'message' => 'Token is valid.',
          'user' => $user,
        ];
      } else {
        return [
          'success' => false,
          'message' => 'Token is invalid or expired.' ,
        ];
      }

    }


    // Returns the current data according to the token
    public function currentUser ()
    {
      $headers = getallheaders();
      $hash = null;

      if ( array_key_exists('authorization', $headers) ) {
        $hash = $headers['authorization'];
      }
      else if ( array_key_exists('Authorization', $headers) ) {
        $hash = $headers['Authorization'];
      }

      $user = self::getTokenUser($hash);

      if ( !$user) {
        return response('', 401);
      }

      return $user;
    }


    public static function getTokenUser( $hash = null ) {
      $userController = new UserController;
      if ( !$hash ) {
        return null;
      }

      $token = Token::where('hash', $hash)->first();

      if ( !$token ) {
        return null;
      }

      return $userController->show($token->user_id);

    }


    public static function is_token_valid($token) {
      $token = Token::where('hash',$token)->first();

      if ( !$token ) {
        return false;
      }

      $expiration = (integer) (new DateTime($token->expiration_date))->format('U');
      $now = (integer) (new DateTime("@".time()))->format('U');

      $is_expired = $expiration < $now ? true : false;
      return $is_expired ? false : true;
    }


    public static function generate_token( $user ) {
      $dateTime = time();
      $expiration = $dateTime + (60 * 60 * .05);

      return Token::create([
        'hash' => bcrypt($dateTime),
        'expiration_date' => new DateTime("@$expiration"), // Expires in 8 hours,
        'role' => $user->role,
        'user_id' => $user->id,
      ]);
    }
}
