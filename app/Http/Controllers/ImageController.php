<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Validator;
use \Input;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Image::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->image;

        $imageName = time().'_'.$image->getClientOriginalName();
        $image->move(public_path('images'), $imageName);
        $imagePath = public_path('images') . '/' . $imageName;
        $image_info = getimagesize($imagePath);
        $image_data = [
            'src' => url('/images') . '/' . $imageName,
            'width' => $image_info[0],
            'height' => $image_info[1],
        ];

        $new_image = new Image($image_data);
        $new_image->save();

        return $new_image;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existing_image = Image::where('id', $id)->first();

        if ( ! $existing_image ) {
            return response([
                'success' => false,
                'message' => 'Image does not exist',
            ], 404);
        }

        return Image::where('id', $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // There seems to be no instance that an image will be editted,
        // So setting up this endpoint won't make sense.
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // There seems to be no instance that an image will be deleted,
        // So setting up this endpoint won't make sense.
    }
}
