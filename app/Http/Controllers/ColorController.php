<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use Validator;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Color::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hex = $request->input('color_value');

        // Validate values
        $validator = Validator::make($request->all(), Color::$post_rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        // Check for existing user
        $existing_color = Color::where('color_value', $hex)->first();
        if ( $existing_color ) {
            return response([
                'success' => false,
                'message' => 'Color value is already taken.',
            ], 409);
        }

        // Otherwise, Create new user
        $new_color = Color::create($request->all());
        return [
            'success' => true,
            'data' => $new_color
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Color::where('id', $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Ensure id is typecasted properly
        $id = (integer) $id;

        $existing_color = Color::where('id', $id)->first();

        // Validate values
        $validator = Validator::make($request->all(), Color::$patch_rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        if ( !$existing_color ) {
            return response([
                'success' => false,
                'message' => 'Unable to update an non-existing color.'
            ], 404);
        }

        $color_value = $request->input('color_value');
        $similar_hex = Color::where('color_value', $color_value)->first();

        if ( $similar_hex && $similar_hex->id !== $id ) {
            return response([
                'success' => false,
                'message' => 'Color value is already taken.'
            ], 409);
        }

        if ( $existing_color->update($request->all()) ) {
            return [
                'success' => true,
                'message' => 'Color updated.',
                'data' => $existing_color,
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing_color = Color::where('id', $id)->first();

        if ( !$existing_color ) {
            return response([
                'success' => false,
                'message' => 'Unable to delete an non-existing color.'
            ], 404);
        }

        $is_deleted = Color::destroy($id);

        if ( $is_deleted ) {
            return [
                'success' => true,
                'message' => 'Successfuly deleted color.',
            ];
        }

        return response([
            'success' => false,
            'message' => 'Unable to delete an non-existing color due to an error.',
        ], 404);
    }
}
