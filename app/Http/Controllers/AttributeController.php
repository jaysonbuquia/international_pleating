<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use Validator;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Attribute::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validate values
        $validator = Validator::make($request->all(), Attribute::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        // Create new attribute
        $new_attribute = Attribute::create($request->all());
        return [
            'success' => true,
            'data' => $new_attribute
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Ensure id is typecasted properly
        $id = (integer) $id;

        $existing_attribute = Attribute::where('id', $id)->first();

        // Validate values
        $validator = Validator::make($request->all(), Attribute::$rules);

        // Respond error if fails
        if ($validator->fails()) {
            return response([
                'success' => false,
                'errors' => $validator->errors()
            ], 409);
        }

        if ( !$existing_attribute ) {
            return response([
                'success' => false,
                'message' => 'Unable to update an non-existing attribute.'
            ], 404);
        }

        $attribute_name = $request->input('name');
        $similar_name = Attribute::where('name', $attribute_name)->first();

        if ( $similar_name && $similar_name->id !== $id ) {
            return response([
                'success' => false,
                'message' => 'Attribute name is already taken.'
            ], 409);
        }

        if ( $existing_attribute->update($request->all()) ) {
            return [
                'success' => true,
                'message' => 'Attribute updated.',
                'data' => $existing_attribute,
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existing_attribute = Attribute::where('id', $id)->first();

        if ( !$existing_attribute ) {
            return response([
                'success' => false,
                'message' => 'Unable to delete an non-existing attribute.'
            ], 404);
        }

        $is_deleted = Attribute::destroy($id);

        if ( $is_deleted ) {
            return [
                'success' => true,
                'message' => 'Successfuly deleted attribute.',
            ];
        }

        return response([
            'success' => false,
            'message' => 'Unable to delete an non-existing attribute due to an error.',
        ], 404);
    }
}
