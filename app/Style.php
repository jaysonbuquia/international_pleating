<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    protected $fillable = [
      'name', 'price', 'attribute', 'steps', 'images', 'number', 'category',
    ];


    public static $rules;


    /**
     * Available categories.
     * These are hardcoded values since there is no way to add/delete categories
     */
    public static $categories = [
      'category_1' => 'Category 1',
      'category_2' => 'Category 2',
      'category_3' => 'Category 3',
    ];
}


/**
 * --------------------------------------------------------------------------------------------
 * COMPUTED VALUES
 * --------------------------------------------------------------------------------------------
 */

/**
* Validation rules
*/
Style::$rules = [
  'name' => 'string|required',
  'price' => ['required', 'regex:/^[0-9]*(\.[0-9]*)?$/'],
  'number' => 'integer|required',
  'category' => 'string|required|in:' . implode(',', array_keys(Style::$categories)),
  'attribute' => 'string|required',
  'steps' => 'string|required', // JSON Array of steps
  'images' => 'string', // JSON Array of image ids
];
