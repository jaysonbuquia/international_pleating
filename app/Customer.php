<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_number', 'name', 'email', 'address', 'city', 'state', 'country', 'notes', 'phone', 'contact',
        'terms_code', 'ship_code', 'shipping_number', 'credit_limit',
        'shipping_address_same', 'shipping_address', 'shipping_city', 'shipping_state', 'shipping_country'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    static public $rules = [
        'account_number' => 'numeric|required',
        'name' => 'string|required',
        'email' => 'email|required',
        'address' => 'string',
        'city' => 'string',
        'state' => 'string',
        'contact' => 'string',
        'notes' => 'string',
        'terms_code' => 'string',
        'ship_code' => 'string',
        'shipping_number' => 'numeric',
        'credit_limit' => ['regex:/^[0-9]*(\.[0-9]*)?$/'],
        'shipping_address_same' => 'bool',
        'shipping_address' => 'string',
        'shipping_city' => 'string',
        'shipping_state' => 'string',
        'shipping_country' => 'string',
    ];
}
