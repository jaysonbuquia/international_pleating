<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = [
      'hash', 'expiration_date', 'role', 'user_id',
    ];

    protected $guarded = [
      'updated_at',
      'created_at',
    ];
}
