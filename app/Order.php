<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
      'order_number',
      'style_id',
      'customer_id',
      'color_id',
      'quantity',
      'quantity_shipment', // Should be an array of integers (quantity per shipment)
      'quantity_production',
      'status',
      'order_date',
      'cancel_date',
      'completion_date',
      'customer_po_number',
      'ship_code',
      'terms',
    ];

    static public $rules = [
      'order_number' => 'integer|required',
      'style_id' => 'integer|required',
      'customer_id' => 'integer|required',
      'color_id' => 'integer|required',
      'quantity' => 'integer|required',
      'quantity_production' => 'integer',
      'status' => 'string|required|in:new,production,hold,shipment,done,cancelled',
      'order_date' => 'date|required',
      // 'cancel_date' => 'date',
      // 'completion_date' => 'date',
      'customer_po_number' => 'integer',
      // 'ship_code' => 'string',
      // 'terms' => 'string',
    ];
}
