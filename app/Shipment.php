<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $fillable = [
      'order_id',
      'quantity',
    ];

    static public $rules = [
      'order_id' => 'integer|required',
      'quantity' => 'integer|required',
    ];
}
