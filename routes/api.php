<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

// Returns user data according to the given token
Route::get('/users/me', 'AuthController@currentUser');

Route::resource('/users', 'UserController', ['parameters' => [
    'user' => 'user_id'
]]);

Route::resource('/colors', 'ColorController', ['parameters' => [
    'color' => 'color_id'
]]);

Route::resource('/attributes', 'AttributeController', ['parameters' => [
    'attribute' => 'attribute_id'
]]);

Route::resource('/styles', 'StyleController', ['parameters' => [
    'style' => 'style_id'
]]);

// Customer search
Route::post('/customers/search', 'CustomerController@searchByName');
// Customer resource
Route::resource('/customers', 'CustomerController', ['parameters' => [
    'customer' => 'customer_id'
]]);

Route::resource('/images', 'ImageController', ['parameters' => [
    'image' => 'image_id'
]]);


Route::resource('/orders', 'OrderController', ['parameters' => [
    'order' => 'order_id'
]]);

// Route::get('/orders/{status}/{page?}', 'OrderController@withStatus');



Route::resource('/shipments', 'ShipmentController', ['parameters' => [
    'shipment' => 'shipment_id'
]]);


// Dashbaord api routes
Route::get('/dashboard', 'DashboardController@counts');

// Logs a user in
Route::post('/auth/login', 'AuthController@login');
// Validates a token
Route::post('/auth/validate_token', 'AuthController@validate_token');
