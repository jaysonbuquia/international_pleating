# International Pleating #

## Contributing ##
This application is built in **Laravel**, using **ReactJS** to handle Front End componentization.

### Requirements ###

* Apache 2.x
* PHP 7.x
* [PHP extensions required to run a Lavel 5.3 application.](https://laravel.com/docs/5.3/installation#server-requirements)
* MySQL 5.x
* Node 8.x
* NPM 5.x
* Gulp (installed globally)

### Environment Setup ###

* Clone this repository
* Register your local repository into your virtual host
* Use the domain `http://ip.local` for your hosts file. i.e., `127.0.0.1 ip.local` (This domain is harcoded in the gulpfile for watch)
* Run `composer install`. This will install Laravel 5.3 dependencies.
* Run `npm install`. This will install node packages.
* Create a database for your local environment.
* Ensure that your database configurations are present in the `.env`file.
* Run `php artisan migrate`. This will bootstrap your local database.
* Run `php artisan db:seed --class SuperAdminUserSeeder`. This will create a superadmin user. 
	- Username: `superadmin`
	- Password: `super@admin.com`
* If everything works fine, you should be able to view the site at `http://ip.local`.


### Development ###

* Make sure that you are checked out in the `develop` branch.
* Prepend this value to the `.env` file: `ENV=LOCAL`. This will ensure that gulp tasks will run within a local environment context.
* Run `gulp watch` to start a browserSync server and watch for changes.
* To manually build assets, run `gulp`.
* Styles are preprocessed using SCSS. Stylesheet files are located inside `src/styles`.
* Components are built using ReactJS. They are located inside `src/components`.

### Database Seeds ###

If you need sample data, you can run these commands to seed different types of entities:  

* `php artisan db:seed --class ColorSeeder`
* `php artisan db:seed --class CustomerSeeder`
* `php artisan db:seed --class ImageSeeder`
* `php artisan db:seed --class StyleSeeder`
* `php artisan db:seed --class AttributeSeeder`
* `php artisan db:seed --class OrderSeeder`

### DEV Environment ###

The DEV environment can be accessed at [159.65.47.243](http://159.65.47.243).
You need to manually deploy updates through SSH, since there is no autodeploy integrated for this environment.
Run `git pull origin develop` to trigger deploy.  

For SSH access, contact `buquia.jace@gmail.com` to add your SSH key into the DEV server.
Once granted access, you should be able to SSH into it: `ssh root@159.65.47.243`

### Issues ###

For issues during setup or development workflow, send an email to `buquia.jace@gmail.com`.
