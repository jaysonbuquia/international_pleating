let { isDevelopment, isLocal, isStaging, isProduction, isWatching,
      projectRoot, browserSync } = require('gulp-tasks-preset');

let gulp = require('gulp');
let src = projectRoot('src/fonts/**/*.*');
let dest = 'public/fonts';

module.exports = {

  fn: async function () {
    return gulp.src(src)
        .pipe(gulp.dest(dest));
  },

  watchFiles: projectRoot(src),

}