/**
 * Watch task will only be registered when gulp is ran in watch mode, i.e., `gulp watch`
 * No need to register this task in gulpfile.
 */

let { tasks, browserSync, projectRoot } = require('gulp-tasks-preset');
let gulp = require('gulp');
let watch = require('gulp-watch');
let watchableTasks = tasks.filter( task => task.watchFiles ? true : false );
let watchDeps = tasks.map( task => task.name );




gulp.task('watch', watchDeps, function () {

  // Start browserSync
  browserSync.init({
    proxy: {
      target: "http://ip.local",
    },
    files: [ projectRoot('public/**/*.css'), projectRoot('public/**/*.js'), projectRoot("**/*.php") ]
  }, () => { browserSync.initialized = true; });

  // Watch files
  for ( let task of watchableTasks ) {
    task.fn.call();
    gulp.watch(task.watchFiles, () => {
      // task.fn();
      gulp.start(task.name);
    });
  }

});