<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('order_number');
            $table->integer('quantity');
            $table->string('status'); // [ new, processing, printing, shipping, shipped ]
            $table->integer('styles_id'); // reference to {Style}
            $table->integer('customer_id'); // reference to {Customer}
            $table->integer('color_id'); // reference to {Color}
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
