<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingColumnsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('terms_code')->nullable();
            $table->string('ship_code')->nullable();
            $table->integer('shipping_number')->nullable();
            $table->float('credit_limit')->nullable();
            // shipping
            $table->boolean('shipping_address_same')->nullable()->default(TRUE);
            $table->string('shipping_address')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumns(['terms_code', 'ship_code', 'shipping_number', 'credit_limit', 'shipping_address_same', 'shipping_address', 'shipping_city', 'shipping_state', 'shipping_country' ]);
        });
    }
}
