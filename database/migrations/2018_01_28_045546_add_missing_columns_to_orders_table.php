<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('order_date');
            $table->dateTime('cancel_date')->nullable();
            $table->dateTime('completion_date')->nullable();
            $table->integer('customer_po_number')->nullable();
            $table->string('ship_code')->nullable();
            $table->string('terms')->nullable();
            // Make nullable status
            $table->string('status')->nullable()->change(); // [ new, production, shipment, done, canceled ]
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumns([ 'order_date', 'cancel_date', 'completion_date', 'customer_po_number', 'ship_code', 'terms', 'status' ]);
            $table->string('status')->change();
        });
    }
}
