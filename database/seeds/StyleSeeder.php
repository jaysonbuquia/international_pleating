<?php
use App\Style;
use Illuminate\Database\Seeder;

class StyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $styles = [
          [
            'name' => 'Style ABC',
            'price' => 100.23,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([1, 2, 3]),
            'number' => 237,
            'category' => 'test_category',
          ],
          [
            'name' => 'Style DEF',
            'price' => 341,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([4, 5, 6, 1]),
            'number' => 237,
            'category' => 'test_category',
          ],
          [
            'name' => 'Style GHI',
            'price' => 100.23,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([2, 4]),
            'number' => 237,
            'category' => 'test_category',
          ],
          [
            'name' => 'Style JKL',
            'price' => 12,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([1,3]),
            'number' => 237,
            'category' => 'test_category',
          ],
          [
            'name' => 'Style MNO',
            'price' => 10,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([5,4,3]),
            'number' => 237,
            'category' => 'test_category',
          ],
          [
            'name' => 'Style PQR',
            'price' => 20,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([3,2,1,5]),
            'number' => 237,
            'category' => 'test_category',
          ],
          [
            'name' => 'Style STU',
            'price' => 21,
            'attribute' => 'SSS',
            'steps' => json_encode([
                          [
                            'text' => 'Step 101',
                            'subtext' => '10 min',
                          ],
                          [
                            'text' => 'Step 102',
                            'subtext' => '10 min',
                          ]
                        ]),
            'images' => json_encode([5,2,1,3,4]),
            'number' => 237,
            'category' => 'test_category',
          ]
        ];

        foreach ($styles as $style) {
          Style::create($style);
        }
    }
}
