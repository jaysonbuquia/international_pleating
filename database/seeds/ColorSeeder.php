<?php
use App\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
          [
            'color_name' => 'REd',
            'color_number' => 3534534,
            'color_value' => '#FF0000',
          ],
          [
            'color_name' => 'Blue',
            'color_number' => 3534534,
            'color_value' => '#0000FF',
          ],
          [
            'color_name' => 'Green',
            'color_number' => 3534534,
            'color_value' => '#00FF00',
          ],
          [
            'color_name' => 'Aqua',
            'color_number' => 3534534,
            'color_value' => '#00FFFF',
          ],
          [
            'color_name' => 'Navy',
            'color_number' => 3534534,
            'color_value' => '#000080',
          ],
          [
            'color_name' => 'Maroon',
            'color_number' => 3534534,
            'color_value' => '#800000',
          ],
          [
            'color_name' => 'White',
            'color_number' => 3534534,
            'color_value' => '#000000',
          ],
        ];

        foreach ($colors as $color) {
          Color::create($color);
        }
    }
}
