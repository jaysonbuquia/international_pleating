<?php
use App\Image;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = [
          [
            'src' => 'https://placeimg.com/640/480/any',
            'title' => 'Placeholder image',
            'alt' => '',
            'width' => 640,
            'height' => 480,
          ],
          [
            'src' => 'https://placeimg.com/1280/800/any',
            'title' => 'Placeholder image',
            'alt' => '',
            'width' => 1280,
            'height' => 800,
          ],
          [
            'src' => 'https://placeimg.com/1920/900/any',
            'title' => 'Placeholder image',
            'alt' => '',
            'width' => 1920,
            'height' => 900,
          ],
          [
            'src' => 'https://placeimg.com/768/1024/any',
            'title' => 'Placeholder image',
            'alt' => '',
            'width' => 768,
            'height' => 1024,
          ],
          [
            'src' => 'https://placeimg.com/800/400/any',
            'title' => 'Placeholder image',
            'alt' => '',
            'width' => 800,
            'height' => 400,
          ],
          [
            'src' => 'https://placeimg.com/400/800/any',
            'title' => 'Placeholder image',
            'alt' => '',
            'width' => 400,
            'height' => 800,
          ],
        ];

        foreach ($images as $image) {
          Image::create($image);
        }
    }
}
