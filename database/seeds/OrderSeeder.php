<?php
use App\Order;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = [
          [
            'order_number' => 999,
            'style_id' => 5,
            'customer_id' => 2,
            'color_id' => 3,
            'quantity' => 2,
            'status' => 'new',
            'order_date' => '2017-12-13',
            'customer_po_number' => 934,
            'ship_code' => 'ABC123',
            'terms' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque natus officia quibusdam!',
          ],
          [
            'order_number' => 345,
            'style_id' => 2,
            'customer_id' => 3,
            'color_id' => 1,
            'quantity' => 12,
            'status' => 'production',
            'quantity_production' => 12,
            'order_date' => '2017-12-15',
            'customer_po_number' => 934,
            'ship_code' => 'ABC124',
            'terms' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque natus officia quibusdam!',
          ],
          [
            'order_number' => 013,
            'style_id' => 2,
            'customer_id' => 8,
            'color_id' => 2,
            'quantity' => 9,
            'status' => 'new',
            'order_date' => '2017-12-16',
            'customer_po_number' => 910,
            'ship_code' => 'SOE103',
            'terms' => 'Lorem ipsum dolor sit amet.',
          ],
          [
            'order_number' => 193,
            'style_id' => 7,
            'customer_id' => 1,
            'color_id' => 3,
            'quantity' => 1,
            'status' => 'new',
            'order_date' => '2017-12-12',
            'customer_po_number' => 192,
            'ship_code' => 'PED872',
            'terms' => 'Lorem ipsum dolor sit amet.',
          ],
          [
            'order_number' => 294,
            'style_id' => 8,
            'customer_id' => 2,
            'color_id' => 5,
            'quantity' => 7,
            'status' => 'new',
            'order_date' => '2017-12-16',
            'customer_po_number' => 128,
            'ship_code' => 'SPQ031',
          ],
          [
            'order_number' => 286,
            'style_id' => 1,
            'customer_id' => 4,
            'color_id' => 6,
            'quantity' => 10,
            'status' => 'shipment',
            'quantity_shipment' => 10,
            'order_date' => '2017-12-19',
            'customer_po_number' => 267,
            'ship_code' => 'EMI234',
          ],
          [
            'order_number' => 999,
            'style_id' => 5,
            'customer_id' => 2,
            'color_id' => 3,
            'quantity' => 2,
            'status' => 'done',
            'order_date' => '2017-12-13',
            'customer_po_number' => 934,
            'ship_code' => 'ABC123',
            'terms' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque natus officia quibusdam!',
          ],
          [
            'order_number' => 345,
            'style_id' => 2,
            'customer_id' => 3,
            'color_id' => 1,
            'quantity' => 12,
            'status' => 'production',
            'quantity_production' => 12,
            'order_date' => '2017-12-15',
            'customer_po_number' => 934,
            'ship_code' => 'ABC124',
            'terms' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque natus officia quibusdam!',
          ],
          [
            'order_number' => 013,
            'style_id' => 2,
            'customer_id' => 8,
            'color_id' => 2,
            'quantity' => 9,
            'status' => 'done',
            'order_date' => '2017-12-16',
            'customer_po_number' => 910,
            'ship_code' => 'SOE103',
            'terms' => 'Lorem ipsum dolor sit amet.',
          ],
          [
            'order_number' => 193,
            'style_id' => 7,
            'customer_id' => 1,
            'color_id' => 3,
            'quantity' => 1,
            'status' => 'cancelled',
            'order_date' => '2017-12-12',
            'customer_po_number' => 192,
            'ship_code' => 'PED872',
            'terms' => 'Lorem ipsum dolor sit amet.',
          ],
          [
            'order_number' => 294,
            'style_id' => 8,
            'customer_id' => 2,
            'color_id' => 5,
            'quantity' => 7,
            'quantity_production' => 3,
            'status' => 'hold',
            'order_date' => '2017-12-16',
            'customer_po_number' => 128,
            'ship_code' => 'SPQ031',
          ],
          [
            'order_number' => 286,
            'style_id' => 1,
            'customer_id' => 4,
            'color_id' => 6,
            'quantity' => 10,
            'quantity_production' => 5,
            'status' => 'hold',
            'order_date' => '2017-12-19',
            'customer_po_number' => 267,
            'ship_code' => 'EMI234',
          ],
        ];

        Order::truncate();

        foreach ($orders as $order) {
          Order::create($order);
        }
    }
}
