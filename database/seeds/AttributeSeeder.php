<?php
use App\Attribute;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [
          [
            'name' => 'Attribute 1',
          ],
          [
            'name' => 'Attribute 2',
          ],
          [
            'name' => 'Attribute 3',
          ],
          [
            'name' => 'Attribute 4',
          ],
          [
            'name' => 'Attribute 5',
          ],
          [
            'name' => 'Attribute 6',
          ],
          [
            'name' => 'Attribute 7',
          ],
        ];

        foreach ($attributes as $attribute) {
          Attribute::create($attribute);
        }
    }
}
