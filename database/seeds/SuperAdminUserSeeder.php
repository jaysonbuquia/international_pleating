<?php
use App\User;
use Illuminate\Database\Seeder;

class SuperAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'first_name' => 'Super',
          'last_name' => 'Admin',
          'email' => 'super@admin.com',
          'password' => 'superadmin',
          'photo_id' => 1,
          'role' => 'superadmin',
        ]);
    }
}
