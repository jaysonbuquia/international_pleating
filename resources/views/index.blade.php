<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>International Pleating</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="/css/main.css">
</head>
<body>
  <div id="root"></div>
  <script src="/js/app.js"></script>
</body>
</html>